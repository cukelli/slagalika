    package com.example.slagalika.firebase;
    
    import static android.content.ContentValues.TAG;
    
    import android.content.Intent;
    import android.util.Log;
    
    import androidx.annotation.NonNull;
    
    import com.example.slagalika.activity.LoginActivity;
    import com.example.slagalika.activity.RegistrationActivity;
    import com.example.slagalika.model.GameSession;
    import com.google.firebase.database.DataSnapshot;
    import com.google.firebase.database.DatabaseError;
    import com.google.firebase.database.DatabaseReference;
    import com.google.firebase.database.FirebaseDatabase;
    import com.google.firebase.database.ValueEventListener;

    import java.util.ArrayList;
    import java.util.HashMap;
    import java.util.Map;
    import java.util.UUID;
    
    public class FirebaseManager {
        private final DatabaseReference mDatabase;

        public FirebaseManager() {
            // Initialize the database
            mDatabase = FirebaseDatabase.getInstance().getReference();
        }

        public void  getUsersPoints(String sessionId, final ValueEventListener vl) {
            DatabaseReference pointsRef = mDatabase.child("gameSessions").
                    child(sessionId);
            pointsRef.addValueEventListener(vl);
        }

        public void  getUser(final ValueEventListener vl) {
            DatabaseReference pointsRef = mDatabase.child("users");
            pointsRef.addListenerForSingleValueEvent(vl);
        }
    
        public GameSession createGameSession(String username1, String username2) {
            DatabaseReference pointsRef = mDatabase.child("gameSessions");
            Map<String,Integer> pointsMap = new HashMap<>();
            pointsMap.put(username1,0);
            pointsMap.put(username2,0);
            String key = UUID.randomUUID().toString();
            GameSession gameSession = new GameSession(key,pointsMap);
            pointsRef.child(key).push().setValue(gameSession)
                    .addOnSuccessListener(e -> {
    
                    }).addOnFailureListener(e ->
                            Log.e(TAG, "Error writing data to the database: " + e.getMessage()));
            return gameSession;
    
        }

        public void updateGameSessionPoints(String sessionId, Map<String,Integer> usernamesIn) {
            DatabaseReference gameSessionRef = mDatabase.child("gameSessions").
                    child(sessionId);
            gameSessionRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {

                            GameSession gameSession = childSnapshot.getValue(GameSession.class);
                            if (gameSession != null) {
                                gameSession.setUsernames(usernamesIn);
                                Map<String,Object> valueMap = new HashMap<>();
                                valueMap.put(childSnapshot.getKey(),gameSession);
                                gameSessionRef
                                        .updateChildren(valueMap)
                                        .addOnSuccessListener(e -> {
                                            Log.d(TAG, "Points updated successfully");
                                        })
                                        .addOnFailureListener(e -> {
                                            Log.e(TAG, "Error updating points: "
                                                    + e.getMessage());
                                        });
                            } else {
                                Log.e(TAG, "Failed to retrieve game session data");
                            }
                        }
                        } else{
                            Log.e(TAG, "Game session not found");
                        }

                    }


                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(TAG, "Error retrieving game session data: " + databaseError.getMessage());
                }
            });
        }

    }
