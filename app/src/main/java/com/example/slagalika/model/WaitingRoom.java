package com.example.slagalika.model;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class WaitingRoom {

    private DatabaseReference waitingRoomRef;

    public WaitingRoom() {
        waitingRoomRef = FirebaseDatabase.getInstance().getReference().child("waitingRoom");
    }

    public void joinWaitingRoom(String userId) {
        waitingRoomRef.child(userId).setValue(true);
    }


}
