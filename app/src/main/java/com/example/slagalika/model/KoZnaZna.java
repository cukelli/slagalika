package com.example.slagalika.model;

import java.util.List;

public class KoZnaZna extends Game {
    private int currentQuestionIndex;


    public KoZnaZna(int player1Score,int player2Score,
                    List<QuestionAndAnswer> questionsAndAnswers) {
        super(player1Score, player2Score);
        this.questionsAndAnswers = questionsAndAnswers;
    }

    private List<QuestionAndAnswer> questionsAndAnswers;

    public List<QuestionAndAnswer> getQuestionsAndAnswers() {
        return questionsAndAnswers;
    }

    public void setQuestionsAndAnswers(List<QuestionAndAnswer> questionsAndAnswers) {
        this.questionsAndAnswers = questionsAndAnswers;
    }

    public void setCurrentQuestionIndex(int currentQuestionIndex) {
        this.currentQuestionIndex = currentQuestionIndex;
    }

    public int getCurrentQuestionIndex() {
        return currentQuestionIndex;
    }

    public void checkAnswer(int selectedAnswerIndex) {
        QuestionAndAnswer currentQuestion = questionsAndAnswers.get(currentQuestionIndex);

        if (currentQuestion.isAnswerCorrect(selectedAnswerIndex)) {
            int points = 10;
            setPlayer1Score(getPlayer2Score() + 10);
        } else {
            int points = 5;
            setPlayer1Score(getPlayer2Score() - 5);
        }

        // Move to the next question
        currentQuestionIndex = (currentQuestionIndex + 1) % questionsAndAnswers.size();
    }

}

