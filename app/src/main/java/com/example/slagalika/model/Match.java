package com.example.slagalika.model;
//Partija igre - jedna cela igra izmedju 2 igraca (sve igre)

public class Match {
    private int gamePartyId;
    private RegisteredUser user1;
    private RegisteredUser user2;

    public Match(int gamePartyId, RegisteredUser user1,RegisteredUser user2) {
        this.gamePartyId = gamePartyId;
        this.user1 = user1;
        this.user2 = user2;
    }

    public int getGamePartyId() {
        return gamePartyId;
    }

    public void setGamePartyId(int gamePartyId) {
        this.gamePartyId = gamePartyId;
    }

    public RegisteredUser getUser1() {
        return user1;
    }

    public void setUser1(RegisteredUser user1) {
        this.user1 = user1;
    }

    public RegisteredUser getUser2() {
        return user2;
    }

    public void setUser2(RegisteredUser user2) {
        this.user2 = user2;
    }
}
