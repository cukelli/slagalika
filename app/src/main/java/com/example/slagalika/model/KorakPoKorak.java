package com.example.slagalika.model;

import java.util.ArrayList;
import java.util.LinkedList;

public class KorakPoKorak extends Game {

    private String answer;
    private LinkedList<String> words;

    public KorakPoKorak(String answer, LinkedList<String> words) {
        this.answer = answer;
        this.words = words;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public LinkedList<String> getWords() {
        return words;
    }

    public void setWords(LinkedList<String> words) {
        this.words = words;
    }
}
