package com.example.slagalika.model;

import java.util.ArrayList;

public class Skocko extends Game {

    private ArrayList<String> correctCombination;


    public Skocko(int player1Score,int player2Score,ArrayList<String> correctCombination) {
        super(player1Score, player2Score);
        this.correctCombination = correctCombination;
    }
    public ArrayList<String> getCorrectCombination() {
        return correctCombination;
    }

    public void setCorrectCombination(ArrayList<String> correctCombination) {
        this.correctCombination = correctCombination;
    }
}
