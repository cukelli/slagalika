package com.example.slagalika.model;

import com.example.slagalika.enumeration.Field;

public class PairSpojnice {

    private int id;
    private String word1;
    private String word2;
    private boolean leftWordConnected;
    private boolean rightWordConnected;
    private int color;

    public PairSpojnice(int id,String word1,String word2) {
        this.id = id;
        this.word1 = word1;
        this.word2 = word2;
    }

    public PairSpojnice() {}

    public PairSpojnice(String word1, String word2) {
        this.word1 = word1;
        this.word2 = word2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWord1() {
        return word1;
    }

    public void setWord1(String word1) {
        this.word1 = word1;
    }

    public String getWord2() {
        return word2;
    }

    public void setWord2(String word2) {
        this.word2 = word2;
    }

    public boolean isLeftWordConnected() {
        return leftWordConnected;
    }

    public void setLeftWordConnected(boolean leftWordConnected) {
        this.leftWordConnected = leftWordConnected;
    }

    public boolean isRightWordConnected() {
        return rightWordConnected;
    }

    public void setRightWordConnected(boolean rightWordConnected) {
        this.rightWordConnected = rightWordConnected;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
