package com.example.slagalika.model;
public class Game {
    private int player1Score;
    private int player2Score;

    public Game() {}
    public Game( int player1Score,int player2Score) {
        this.player2Score = player2Score;
        this.player1Score = player1Score;
    }

    public int getPlayer1Score() {
        return player1Score;
    }
    public void setPlayer1Score(int player1Score) {
        this.player1Score = player1Score;
    }

    public int getPlayer2Score() {
        return player2Score;
    }

    public void setPlayer2Score(int player2Score) {
        this.player2Score = player2Score;
    }

}
