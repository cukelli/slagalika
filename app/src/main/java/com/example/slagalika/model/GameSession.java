package com.example.slagalika.model;

import java.util.HashMap;
import java.util.Map;

//partija
public class GameSession {
    private String id;
    private Map<String, Integer> usernames; //username i broj bodova (inicijalno je 0)

    public GameSession() {
        usernames = new HashMap<>();
    }

    public GameSession(String id, Map<String, Integer> usernames) {
        this.id = id;
        this.usernames = usernames;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Integer> getUsernames() {
        return usernames;
    }

    public void setUsernames(Map<String, Integer> usernames) {
        this.usernames = usernames;
    }


}
