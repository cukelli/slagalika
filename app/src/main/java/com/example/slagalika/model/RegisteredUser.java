package com.example.slagalika.model;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class RegisteredUser {
    private String email;
    private int stars;
    private String imagePath;
    private int tokens;
    private String password;
    private String username;

    public RegisteredUser(){

    }
    public RegisteredUser(String email,int stars,int tokens,
                          String imagePath,String password, String username) {
        this.email = email;
        this.stars = stars;
        this.tokens = tokens;
        this.password = password;
        this.username = username;
        this.imagePath = imagePath;
    }




    public RegisteredUser(String username,int stars){
        this.username = username;
        this.stars = stars;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public String getImagePath() {
        return imagePath;
    }
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public int getTokens() {
        return tokens;
    }

    public void setTokens(int tokens) {
        this.tokens = tokens;
    }
}
