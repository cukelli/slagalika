package com.example.slagalika.model;

import java.util.List;

public class Spojnice extends Game {

      private List<PairSpojnice> pairsList;

    public Spojnice(int player1Score,int player2Score,
       List<PairSpojnice> pairsList) {
        super(player1Score, player2Score);
        this.pairsList = pairsList;
    }
    public Spojnice() {}

    public List<PairSpojnice> getPairsList() {
        return pairsList;
    }

    public void setPairsList(List<PairSpojnice> pairsList) {
        this.pairsList = pairsList;
    }
}
