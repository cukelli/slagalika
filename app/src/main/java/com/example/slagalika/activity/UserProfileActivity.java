package com.example.slagalika.activity;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.slagalika.R;
import com.example.slagalika.databinding.ActivityRegisteredUserHomePageBinding;
import com.example.slagalika.databinding.ActivityUserProfileBinding;
import com.example.slagalika.firebase.FirebaseManager;
import com.example.slagalika.model.GameSession;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class UserProfileActivity extends AppCompatActivity {


    FirebaseAuth auth;
    Button button;
    TextView textViewEmailUser;

    private DatabaseReference mDatabase;

    private FirebaseManager firebaseManager;

    private String userUsername;

    private String userEmail;

    private float wins = 0;
    private float losses = 0;

    private float wonGamesPercent;

    private float lostGamesPercent;

    private float statisticsCounter = 0;

    FirebaseUser user;

    private ActivityUserProfileBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        binding = ActivityUserProfileBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setItemIconTintList(null);


        textViewEmailUser = findViewById(R.id.textViewEmailUser);
        auth = FirebaseAuth.getInstance();
        button = findViewById(R.id.buttonLogout);
        user = auth.getCurrentUser();
        userEmail = user.getEmail();


        firebaseManager = new FirebaseManager();
        ValueEventListener vl = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot childSnapshot : snapshot.getChildren()) {
                        String email = childSnapshot.child("email").
                                getValue(String.class);
                        if (email.equalsIgnoreCase(userEmail)) {
                            TextView usernameT = findViewById(R.id.editTextUsername);

                            userUsername = childSnapshot.child("username").
                                    getValue(String.class);
                            usernameT.setText(userUsername);
                            break;
                        }
                    }
                } else {
                    Log.e(TAG, "Failed to retrieve game session data");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Handle onCancelled event
            }
        };
        firebaseManager.getUser(vl);
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        DatabaseReference gameSessionRef = mDatabase.child("gameSessions");
        ValueEventListener vl2 =new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        for (DataSnapshot innerChildSnapshot : childSnapshot.getChildren()) {
                            GameSession gameSession = innerChildSnapshot.getValue(GameSession.class);
                            if (gameSession != null) {
                                if (!gameSession.getUsernames().containsKey(userUsername)){
                                    continue;
                                 }
                                statisticsCounter++;
                                int pointsCurrent = 0;
                                int pointsOpponent = 0;
                                for (Map.Entry<String,Integer> entry :
                                        gameSession.getUsernames().entrySet()) {


                                   String currentUsername = entry.getKey();
                                   Integer points = entry.getValue();
                                   if (currentUsername.equalsIgnoreCase(userUsername)){
                                       pointsCurrent = points;
                                   } else {
                                       pointsOpponent = points;
                                   }

                                }
                                if (pointsCurrent > pointsOpponent){
                                    wins++;
                                } else {
                                    losses++;
                                }

                            } else {
                                Log.e(TAG, "Failed to retrieve game session data");
                            }
                        }
                    }
                } else{
                    Log.e(TAG, "Game session not found");
                }

                System.out.println("wins:" + wins);
                System.out.println("losses" + losses);
                System.out.println("Played games: " + statisticsCounter);

                wonGamesPercent = (wins / statisticsCounter) * 100;
                lostGamesPercent = (losses / statisticsCounter) * 100;

                wonGamesPercent = (float) Math.round(wonGamesPercent*100) / 100;
                lostGamesPercent = (float) Math.round(lostGamesPercent*100) / 100;

                TextView wonGames = findViewById(R.id.textView24);
                wonGames.setTypeface(wonGames.getTypeface(), Typeface.BOLD);
                wonGames.setText(String.valueOf(wins));

                TextView lostGames = findViewById(R.id.textView18);
                lostGames.setTypeface(lostGames.getTypeface(), Typeface.BOLD);
                lostGames.setText(String.valueOf(losses));

                TextView playedGames = findViewById(R.id.playedGamesNumber);
                playedGames.setTypeface(playedGames.getTypeface(), Typeface.BOLD);
                playedGames.setText(String.valueOf(statisticsCounter));

                TextView wonGamesPercentT = findViewById(R.id.wonGamesPercent);
                wonGamesPercentT.setTypeface(wonGamesPercentT.getTypeface(), Typeface.BOLD);
                wonGamesPercentT.setText(wonGamesPercentT.getText() +
                        " " + String.valueOf(wonGamesPercent)+"%");

                TextView lostGamesPercentT = findViewById(R.id.lostGamesPercent);
                lostGamesPercentT.setTypeface(lostGamesPercentT.getTypeface(), Typeface.BOLD);
                lostGamesPercentT.setText(lostGamesPercentT.getText() +
                        " " + String.valueOf(lostGamesPercent) +"%");



            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, "Error retrieving game session data: " + databaseError.getMessage());
            }
        };

        gameSessionRef.addValueEventListener(vl2);

        if (user == null) {
            Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(intent);
            finish();

        }
        else {
            textViewEmailUser.setText(user.getEmail());
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
                finish();


            }
        });

        TextView fieldUsername = findViewById(R.id.textViewName);
        fieldUsername.setTypeface(fieldUsername.getTypeface(), Typeface.BOLD);
        navView.setSelectedItemId(R.id.navigation_profile);
        navView.setItemIconTintList(null);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this,
                R.id.nav_host_fragment_activity_user_profile);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);

        MenuItem onlineFriendsItem = binding.navView.getMenu().findItem(R.id.navigation_online_friends);
        MenuItem homeItem = binding.navView.getMenu().findItem(R.id.home);
        System.out.println(homeItem);
        System.out.println(onlineFriendsItem);



        onlineFriendsItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override

            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(UserProfileActivity.this,
                        ActivityOnlineFriends.class);
                startActivity(intent);
                return true;
            }
        });

        homeItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override

            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(UserProfileActivity.this,
                        RegisteredUserHomePageActivity.class);
                startActivity(intent);
                return true;
            }
        });


        TextView fieldEmail = findViewById(R.id.textViewEmail);
        fieldEmail.setTypeface(fieldEmail.getTypeface(), Typeface.BOLD);

        TextView fieldStats = findViewById(R.id.textViewStats);
        fieldStats.setTypeface(fieldStats.getTypeface(), Typeface.BOLD);




        TextView fieldAsocijacije = findViewById(R.id.textViewAsocijacije);
        fieldAsocijacije.setTypeface(fieldAsocijacije.getTypeface(), Typeface.BOLD);



        TextView fieldSkockoPoints = findViewById(R.id.textViewSkockoPoints);
        fieldSkockoPoints.setTypeface(fieldSkockoPoints.getTypeface(), Typeface.ITALIC);

        TextView fieldSkocko = findViewById(R.id.textViewSkocko);
        fieldSkocko.setTypeface(fieldSkocko.getTypeface(), Typeface.BOLD);

        //Ko zna zna
        TextView fieldKoZnaZna = findViewById(R.id.textViewKoznaZna);
        fieldKoZnaZna.setTypeface(fieldKoZnaZna.getTypeface(), Typeface.BOLD);

        TextView fieldKoZnaZnaPoints = findViewById(R.id.textViewKoZnaZnaPoints);
        fieldKoZnaZnaPoints.setTypeface(fieldKoZnaZnaPoints.getTypeface(), Typeface.ITALIC);


        //Moj broj

        TextView fieldMojBroj = findViewById(R.id.textViewMojBroj);
        fieldMojBroj.setTypeface(fieldMojBroj.getTypeface(), Typeface.BOLD);

        TextView fieldMojBrojPoints = findViewById(R.id.textViewMojBrojPoints);
        fieldMojBrojPoints.setTypeface(fieldMojBrojPoints.getTypeface(), Typeface.ITALIC);


        //Korak po korak

        TextView fieldKorakPoKorak = findViewById(R.id.textViewKorakPpoKorak);
        fieldKorakPoKorak.setTypeface(fieldKorakPoKorak.getTypeface(), Typeface.BOLD);

        TextView fieldKorakPoKorakPoints = findViewById(R.id.textViewKorakPoKorakPoints);
        fieldKorakPoKorakPoints.setTypeface(fieldKorakPoKorakPoints.getTypeface(), Typeface.ITALIC);



    }


 }
