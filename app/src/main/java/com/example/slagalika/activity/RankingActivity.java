package com.example.slagalika.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.ListView;

import com.example.slagalika.R;
import com.example.slagalika.adapter.RankingAdapter;
import com.example.slagalika.model.RegisteredUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class RankingActivity extends AppCompatActivity {

    private ListView mRankingListView;

    private RankingAdapter mRankingAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        mRankingListView = findViewById(R.id.ranking_list);

        ArrayList<RegisteredUser> users = new ArrayList<>();
        users.add(new RegisteredUser("Dusko Dugousko", 6));
        users.add(new RegisteredUser("Paja Patak", 4));
        users.add(new RegisteredUser("Elmer Davez", 3));

        mRankingAdapter = new RankingAdapter(this, users);
        mRankingListView.setAdapter(mRankingAdapter);
    }
}