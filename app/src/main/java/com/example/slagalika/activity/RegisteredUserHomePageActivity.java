package com.example.slagalika.activity;

import static android.content.ContentValues.TAG;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.example.slagalika.R;
import com.example.slagalika.firebase.FirebaseManager;
import com.example.slagalika.model.RegisteredUser;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.slagalika.databinding.ActivityRegisteredUserHomePageBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class RegisteredUserHomePageActivity extends AppCompatActivity {

    private ActivityRegisteredUserHomePageBinding binding;
    private DatabaseReference testRef;

    String userEmail;

    String userUsername = "";

    FirebaseManager firebaseManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityRegisteredUserHomePageBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setItemIconTintList(null);
        navView.setSelectedItemId(R.id.home); // set the home item as selected

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        userEmail = currentUser.getEmail();
        firebaseManager = new FirebaseManager();
        ValueEventListener vl = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot childSnapshot : snapshot.getChildren()) {
                        String email = childSnapshot.child("email").
                                getValue(String.class);
                        if (email.equalsIgnoreCase(userEmail)) {
                            userUsername = childSnapshot.child("username").
                                    getValue(String.class);
                            break;
                        }
                    }
                } else {
                    Log.e(TAG, "Failed to retrieve game session data");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Handle onCancelled event
            }
        };
        firebaseManager.getUser(vl);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this,
                R.id.nav_host_fragment_activity_registered_user_home_page);
        NavigationUI.setupActionBarWithNavController(this, navController,
                appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);

        MenuItem profileItem = binding.navView.getMenu().findItem(R.id.navigation_profile);
        MenuItem onlineFriendsItem = binding.navView.getMenu().findItem(R.id.navigation_online_friends);

        onlineFriendsItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override

            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(RegisteredUserHomePageActivity.this,
                        ActivityOnlineFriends.class);
                startActivity(intent);
                return true;
            }
        });

        profileItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(RegisteredUserHomePageActivity.this,
                        UserProfileActivity.class);
                startActivity(intent);
                return true;
            }
        });

        Button playBtn = (Button) findViewById(R.id.playBtn);
        playBtn.setOnClickListener(view -> {

        Intent intent = new Intent(
                RegisteredUserHomePageActivity.this,
                WaitingRoomActivity.class);
        intent.putExtra("username",userUsername);
        startActivity(intent);

        });

        Button ranksBtn = (Button) findViewById(R.id.ranksBtn);
        ranksBtn.setOnClickListener(view -> {

            Intent intent = new Intent(RegisteredUserHomePageActivity.
                    this,RankingActivity.class);
            startActivity(intent);


        });

        testRef = FirebaseDatabase.getInstance().getReference().child("gameSessions");
        System.out.println("testRef" + testRef);

        // Add ValueEventListener to testRef
        testRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Iterate through the children of the dataSnapshot
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    // Get the new value added to the database

                    System.out.println("here");
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

    }
//    @Override
//    public void onPause() {
//        super.onPause();
//        System.out.println("pause");
//        deleteUserFromFirebase();
//
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        System.out.println("stop");
//        deleteUserFromFirebase();
//    }
//
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        System.out.println("destroy");
//        deleteUserFromFirebase();
//    }
//
//    public void deleteUserFromFirebase() {
//        System.out.println("deleted user");
//
//
//        // Remove the user from the waiting room
//        FirebaseAuth mAuth = FirebaseAuth.getInstance();
//        FirebaseUser currentUser = mAuth.getCurrentUser();
//        String userEmail = currentUser.getEmail();
//
//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        DatabaseReference waitingRoomRef = database.getReference("waitingRoom");
//
//        waitingRoomRef.orderByChild("email").equalTo(userEmail)
//                .addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
//                            snapshot.getRef().removeValue()
//                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
//                                        @Override
//                                        public void onSuccess(Void aVoid) {
//                                            System.out.println("success");
//                                        }
//                                    })
//                                    .addOnFailureListener(new OnFailureListener() {
//                                        @Override
//                                        public void onFailure(@NonNull Exception e) {
//                                            // Failed to remove the user from the waiting room
//                                            // Handle the error gracefully
//                                        }
//                                    });
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//                        // Handle the onCancelled event, if needed
//                    }
//                });
//
//    }

}