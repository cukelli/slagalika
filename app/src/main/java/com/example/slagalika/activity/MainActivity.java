package com.example.slagalika.activity;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.example.slagalika.R;
import com.example.slagalika.model.RegisteredUser;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button logPageBtn = (Button) findViewById(R.id.loginPageBtn);
        logPageBtn.setOnClickListener((view -> {
            Intent intent = new Intent(MainActivity.this,LoginActivity.class);
            startActivity(intent);
        }));

        Button regPageBtn = (Button) findViewById(R.id.regPageBtn);
        regPageBtn.setOnClickListener((view -> {
            Intent intent = new Intent(MainActivity.this,RegistrationActivity.class);
            startActivity(intent);

        }));

        Button guestPageBtn = (Button) findViewById(R.id.guestBtn);
        guestPageBtn.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this,GuestHomeActivity.class);
            startActivity(intent);
        });

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
       // RegisteredUser a = new RegisteredUser("email1@example.com", 3, 10, null, "password", "username");
        //databaseReference.push().setValue(a)
          //      .addOnSuccessListener(aVoid -> Log.d(TAG, "Data successfully written to the database"))
            //    .addOnFailureListener(e -> Log.e(TAG, "Error writing data to the database: " + e.getMessage()));

        //System.out.println(databaseReference.getDatabase());

        // Read data from the database
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Process the retrieved data
                if (dataSnapshot.exists()) {
                    // Data exists, retrieve and log it
                    Object data = dataSnapshot.getValue();
                    Log.d(TAG, "Retrieved data: " + data.toString());
                } else {
                    // Data doesn't exist
                    Log.d(TAG, "No data found in the database");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Error occurred while retrieving data
                Log.e(TAG, "Error retrieving data from the database: " + databaseError.getMessage());
            }
        });





    }

}
