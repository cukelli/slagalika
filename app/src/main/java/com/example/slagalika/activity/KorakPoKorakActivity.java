    package com.example.slagalika.activity;
    
    import static android.content.ContentValues.TAG;
    
    import androidx.annotation.NonNull;
    import androidx.appcompat.app.AppCompatActivity;
    
    import android.content.Intent;
    import android.os.Bundle;
    import android.os.Handler;
    import android.util.Log;
    import android.view.View;
    import android.widget.Button;
    import android.widget.EditText;
    import android.widget.ImageView;
    import android.widget.TextView;
    import android.widget.Toast;
    
    import com.example.slagalika.R;
    import com.example.slagalika.firebase.FirebaseManager;
    import com.example.slagalika.model.GameSession;
    import com.example.slagalika.model.KorakPoKorak;
    import com.example.slagalika.model.QuestionAndAnswer;
    import com.google.firebase.auth.FirebaseAuth;
    import com.google.firebase.auth.FirebaseUser;
    import com.google.firebase.database.DataSnapshot;
    import com.google.firebase.database.DatabaseError;
    import com.google.firebase.database.DatabaseReference;
    import com.google.firebase.database.FirebaseDatabase;
    import com.google.firebase.database.ValueEventListener;
    import com.google.gson.Gson;
    
    import org.json.JSONArray;
    import org.json.JSONException;
    import org.json.JSONObject;
    
    import java.net.URISyntaxException;
    import java.util.ArrayList;
    import java.util.LinkedList;
    import java.util.List;
    import java.util.Map;
    
    import io.socket.client.IO;
    import io.socket.client.Socket;
    import io.socket.emitter.Emitter;
    
    public class KorakPoKorakActivity extends AppCompatActivity {
    
        private Integer guestScore = 0;
        private LinkedList<Button> buttons = new LinkedList<>();
        private KorakPoKorak game;
        private boolean gamePlaying = false;
        private Runnable runnableGame;
    
        private Socket socket;
        FirebaseAuth auth;
        FirebaseUser user;
    
        GameSession gameS;
        private ImageView confirmButton;
    
        private Handler handler = new Handler();
    
        private Runnable runnable;
        TextView user1Points;
    
        TextView user2Points;
    
        TextView player1Username;
    
        TextView player2Username;
        TextView correctAnswer;
        TextView timer;
        EditText editTextInput;
        private int gameTime = 70;
        private DatabaseReference wordsRef;
        private boolean answered = false;
        private boolean buttonAnswer = false;
    
        private int round = 0;
        FirebaseManager firebaseManager = new FirebaseManager();
        ArrayList indexList;
    
        private String loggedUsername;
    
        private Runnable insideRunnuble;
    
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_korak_po_korak);
    
            String gameObject = getIntent().getStringExtra("game");
            String indexObject = getIntent().getStringExtra("indexes");
            Gson gson = new Gson();
            gameS = gson.fromJson(gameObject, GameSession.class);
            indexList = gson.fromJson(indexObject,ArrayList.class);
            loggedUsername =  getIntent().getStringExtra("username");
            for (int i = 0; i < 7; i++) {
                String button = "button" + i;
                int resId = getResources().getIdentifier(button, "id", getPackageName());
                Button current = findViewById(resId);
                current.setVisibility(View.INVISIBLE);
            }
    
            for (Map.Entry<String,Integer> entry : gameS.getUsernames().entrySet()) {
    
                int index = indexList.indexOf(entry.getKey());
                System.out.println(entry.getKey());
                if (index == 0){
                    player1Username = findViewById(R.id.player1Username);
    
                    player1Username.setText(entry.getKey());
                }
                else {
                    player2Username = findViewById(R.id.player2Username);
    
                    player2Username.setText(entry.getKey());
                }
            }
            try {
                socket = IO.socket("http://192.168.43.207:3000");
                socket.connect();
    
                JSONObject data = new JSONObject();
                data.put("username",loggedUsername);
                data.put("sessionId",gameS.getId());
                socket.emit("korakpokorakgame", data);
    
            } catch (URISyntaxException | JSONException e) {
                e.printStackTrace();
                System.out.println("socket error");
    
            }
             user1Points = findViewById(R.id.player1Score);
             user2Points = findViewById(R.id.player2Score);
    
            user1Points.setText(String.valueOf(gameS.getUsernames().get(indexList.get(0))));
            user2Points.setText(String.valueOf(gameS.getUsernames().get(indexList.get(1))));
    
             timer = findViewById(R.id.textViewTimer);
             timer.setText("70");
             correctAnswer = findViewById(R.id.textViewSolution);
             editTextInput = findViewById(R.id.editTextSolutionType);
            confirmButton = findViewById(R.id.confirmImageButton);
    
            if (!loggedUsername.equalsIgnoreCase(String.valueOf(indexList.get(0)))) {
                editTextInput.setEnabled(false);
                confirmButton.setEnabled(false);
            }
    
            insideRunnuble = new Runnable() {
                @Override
                public void run() {
                    if (!answered && gameTime != 0) {
                        timer.setText(String.valueOf(gameTime--));
                        handler.postDelayed(this, 1000);
                    }
                }
            };
            runnableGame = new Runnable() {
               @Override
               public void run() {
                   editTextInput.setEnabled(true);
                   confirmButton.setEnabled(true);
                   if (!loggedUsername.equalsIgnoreCase(String.valueOf(indexList.get(0)))
                   && round == 0) {
                       editTextInput.setEnabled(false);
                       confirmButton.setEnabled(false);
                   }
    
                   if (loggedUsername.equalsIgnoreCase(String.valueOf(indexList.get(0)))
                           && round != 0) {
                       editTextInput.setEnabled(false);
                       confirmButton.setEnabled(false);
                   }
    
                   confirmButton = findViewById(R.id.confirmImageButton);
                   confirmButton.setOnClickListener(view -> {
                       int size = game.getWords().size();
                       if (editTextInput.getText().toString().trim().equals(game.getAnswer())) {
                           answered = true;
                           buttonAnswer = true;
                           confirmButton.setEnabled(false);
                           handler.removeCallbacks(runnable);
                           int pointsWon = 0;
                           String usernameToSend = "";
                           if (!loggedUsername.equalsIgnoreCase(String.valueOf(indexList.get(0))) &&
                                   round != 0) {
                               correctAnswer.setText(game.getAnswer());
                               usernameToSend = String.valueOf(indexList.get(1));
                           }
                           if (loggedUsername.equalsIgnoreCase(String.valueOf(indexList.get(0))) &&
                                   round == 0) {
                               correctAnswer.setText(game.getAnswer());
                               usernameToSend = String.valueOf(indexList.get(0));
                           }
                           if (size == 6) {
                               pointsWon = 20;
                           } else if (size == 5) {
                               pointsWon = 18;
                           } else if (size == 4) {
                               pointsWon = 16;
                           } else if (size == 3) {
                               pointsWon = 14;
                           } else if (size == 2) {
                               pointsWon = 12;
                           } else if (size == 1) {
                               pointsWon = 10;
                           } else {
                               pointsWon = 8;
                           }
    
                           try {
                               socket = IO.socket("http://192.168.43.207:3000");
                               socket.connect();
    
                               JSONObject data = new JSONObject();
                               data.put("sessionId",gameS.getId());
                               data.put("username", usernameToSend);
                               data.put("points", pointsWon);
                               socket.emit("korakpokorakPoints", data);
    
                           } catch (URISyntaxException | JSONException e) {
                               e.printStackTrace();
                               System.out.println("socket error");
    
                           }
    
    
    
                           // TODO
                           /*
    
                           Fire an event for updating the points of current logged username user
                           (see past files to help you out)
    
                           Do not update it locally (create event handler
                           in the file for event pointsUpdateKorakpoKorak that you will create in the
                           sockets.js file)
    
                           In that event update points of users (current + new points)
    
                           Do fire of event for this specific logic anywhere in file and the 10 secons
                           playing of the other user when first failed
    
                           */
    
                           if (round == 1) {
    
                               try {
                                   socket = IO.socket("http://192.168.43.207:3000");
                                   socket.connect();
                                   JSONObject data = new JSONObject();
                                   data.put("sessionId",gameS.getId());
                                   socket.emit("nextGame", data);
    
                               } catch (URISyntaxException | JSONException e) {
                                   e.printStackTrace();
                                   System.out.println("socket error");
    
                               }
    
    
                           } else {
                               round++;
                               try {
                                   socket = IO.socket("http://192.168.43.207:3000");
                                   socket.connect();
    
                                   JSONObject data = new JSONObject();
                                   data.put("sessionId",gameS.getId());
                                   data.put("roundNumber", round);
                                   socket.emit("korakpokorakreset", data);
    
                               } catch (URISyntaxException | JSONException e) {
                                   e.printStackTrace();
                                   System.out.println("socket error");
    
                               }
                           }
    
    
                       } else {
                           editTextInput.setText("");
                       }
    
    
                   });
    
    
                   handler.postDelayed(insideRunnuble, 10_000);
    
                   for (int i = 0; i < 7; i++) {
                       String button = "button" + i;
                       int resId = getResources().getIdentifier(button, "id", getPackageName());
                       Button current = findViewById(resId);
                       current.setVisibility(View.INVISIBLE);
                       buttons.add(current);
                   }
               }
           };
            handler.postDelayed(runnableGame, 0);
             runnable = new Runnable() {
                 @Override
                 public void run() {
                     if (!answered) {
                         if (game.getWords().size() != 0) {
                             Button button = buttons.remove();
                             button.setText(game.getWords().remove());
                             button.setVisibility(View.VISIBLE);
                             handler.postDelayed(this, 10000);
                         } else {
                             answered = true;
                             handler.postDelayed(this, 10000);
    
    
                         }
    
                     } else {
                         if (!buttonAnswer) {
                             if ((loggedUsername.equalsIgnoreCase(String.valueOf(indexList.get(0)))
                                     && round == 0) || (!loggedUsername.equalsIgnoreCase(String.valueOf(indexList.get(0)))
                                     && round != 0)) {
    
                                 try {
                                     socket = IO.socket("http://192.168.43.207:3000");
                                     socket.connect();
    
                                     JSONObject data = new JSONObject();
                                     data.put("sessionId", gameS.getId());
                                     data.put("roundNumber", round);
                                     socket.emit("korakpokoraksecondtry", data);
    
                                 } catch (URISyntaxException | JSONException e) {
                                     e.printStackTrace();
                                     System.out.println("socket error");
    
                                 }
                             }
                         }
                     }
                 }
             };
    
             handler.postDelayed(runnable, 10000);
    
            socket.on("korakpokorakdata", readKorakPoKorak);
            socket.on("korakpokorakresetgame", resetGame);
            socket.on("korakpokoraksecond", timeOut);
            socket.on("korakpokorakPointsupdate", pointsUpdate);
            socket.on("nextGameSend", newGame);
    
        }
    
    
    
        private Emitter.Listener readKorakPoKorak = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try {
                    String answer = data.getString("answer");
                    LinkedList<String> words = new LinkedList<>();
                    JSONArray wordsArray = data.getJSONArray("words");;
                    System.out.println(answer);
    
                    for (int i = 0; i < wordsArray.length(); i++) {
                        String userObject = wordsArray.getString(i);
                        words.add(userObject);
                    }
                    game = new KorakPoKorak(answer,words);
    
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
    
            }
        };
    
        private Emitter.Listener pointsUpdate = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try {
                    int pointsUpdated = data.getInt("points");
                    int position = indexList.indexOf(data.getString("username"));
                    String usernamePassed = data.getString("username");
                    System.out.println("position: " + position);
                    System.out.println("Username: " + usernamePassed);
                    System.out.println("points update:" + pointsUpdated);
                    runOnUiThread(new Runnable() {
    
                        @Override
                        public void run() {
                           if (position == 0) {
                               user1Points = findViewById(R.id.player1Score);
                               int currentPoints = gameS.getUsernames().get(usernamePassed);
                               gameS.getUsernames().put((String) indexList.get(0),currentPoints
                                       + pointsUpdated);
                               user1Points.setText(String.valueOf(gameS.getUsernames().
                                       get(usernamePassed)));
                           }
                           else {
                               user2Points = findViewById(R.id.player2Score);
                               int currentPoints = gameS.getUsernames().get(usernamePassed);
                               gameS.getUsernames().put((String) indexList.get(1),currentPoints
                                       + pointsUpdated);
                               user2Points.setText(String.valueOf(gameS.getUsernames().
                                       get(usernamePassed)));
    
                           }
    
                        }
                    });
    
    
    
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
    
    
    
            }
        };
        private Emitter.Listener newGame = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Intent intent = new Intent(KorakPoKorakActivity.this,
                        ActivityMojBroj.class);
                GameSession gameSend = new GameSession(gameS.getId(), gameS.getUsernames());
    
                Gson gson = new Gson();
                String gameObject = gson.toJson(gameSend);
                intent.putExtra("game", gameObject);
    
                gson = new Gson();
                String indexListSend = gson.toJson(indexList);
                intent.putExtra("indexes", indexListSend);
                intent.putExtra("username", getIntent().getStringExtra("username"));
                firebaseManager.updateGameSessionPoints(gameS.getId(), gameSend.getUsernames());
                startActivity(intent);
    
    
            }
        };
    
        private Emitter.Listener timeOut = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                handler.removeCallbacks(runnable);
                handler.removeCallbacks(runnableGame);
                handler.removeCallbacks(insideRunnuble);
                JSONObject data = (JSONObject) args[0];
                try {
                    round = data.getInt("roundNumber");
                    System.out.println(round);
    
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
    
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        timer.setText("10");
                        editTextInput.setEnabled(true);
                        confirmButton.setEnabled(true);
                        if (!loggedUsername.equalsIgnoreCase(String.valueOf(indexList.get(0)))
                                && round != 0) {
                            editTextInput.setEnabled(false);
                            confirmButton.setEnabled(false);
                        }
                        if (loggedUsername.equalsIgnoreCase(String.valueOf(indexList.get(0)))
                                && round == 0) {
                            editTextInput.setEnabled(false);
                            confirmButton.setEnabled(false);
    
                        }
                    }
                });
    
                    answered = false;
                    buttonAnswer = false;
                    gameTime = 10;
    
    
                    confirmButton = findViewById(R.id.confirmImageButton);
                    confirmButton.setOnClickListener(view -> {
                        if (editTextInput.getText().toString().trim().
                                equals(game.getAnswer())) {
                            confirmButton.setEnabled(false);
                            gameTime = 0;
                            correctAnswer.setText(game.getAnswer());
                            String usernameToSend = "";
                            if (round == 0) {
                                usernameToSend = String.valueOf(indexList.get(1));
                            }
                            if (round != 0) {
                                usernameToSend = String.valueOf(indexList.get(0));
                            }
    
                            try {
    
                                socket = IO.socket("http://192.168.43.207:3000");
                                socket.connect();
    
                                JSONObject dataSend = new JSONObject();
                                dataSend.put("sessionId", gameS.getId());
                                dataSend.put("username", usernameToSend);
                                dataSend.put("points", 5);
                                socket.emit("korakpokorakPoints", dataSend);
    
    
                            } catch (URISyntaxException | JSONException e) {
                                e.printStackTrace();
                                System.out.println("socket error");
    
                            }
    
    
                        } else {
                            editTextInput.setText("");
                        }
    
                    });
    
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (gameTime != 0) {
                        timer.setText(String.valueOf(gameTime--));
                        handler.postDelayed(this, 1000);
                    } else {
                        if (round == 1) {
                            try {
                                socket = IO.socket("http://192.168.43.207:3000");
                                socket.connect();
                                JSONObject data = new JSONObject();
                                data.put("sessionId",gameS.getId());
                                socket.emit("nextGame", data);
    
                            } catch (URISyntaxException | JSONException e) {
                                e.printStackTrace();
                                System.out.println("socket error");
    
                            }
    
                        } else {
                                round++;
                                handler.removeCallbacks(this);
                                handler.removeCallbacks(runnable);
                            if ((!loggedUsername.equalsIgnoreCase(String.valueOf(indexList.get(0)))
                                    && round == 0) || (loggedUsername.equalsIgnoreCase(String.valueOf(indexList.get(0)))
                                    && round != 0)) {
    
                                try {
                                    editTextInput.setEnabled(false);
                                    confirmButton.setEnabled(false);
    
                                    socket = IO.socket("http://192.168.43.207:3000");
                                    socket.connect();
    
                                    JSONObject data = new JSONObject();
                                    data.put("sessionId", gameS.getId());
                                    data.put("roundNumber", round);
                                    socket.emit("korakpokorakreset", data);
    
    
                                } catch (URISyntaxException | JSONException e) {
                                    e.printStackTrace();
                                    System.out.println("socket error");
    
                                }
                            }
    
                        }
                    }
                }
            }, 0);
    
            }
        };
        private Emitter.Listener resetGame = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                handler.removeCallbacks(runnable);
                handler.removeCallbacks(insideRunnuble);
                handler.removeCallbacks(runnableGame);
                buttons = new LinkedList<>();
                JSONObject data = (JSONObject) args[0];
                try {
                    String answer = data.getString("answer");
                    LinkedList<String> words = new LinkedList<>();
                    JSONArray wordsArray = data.getJSONArray("words");;
                    System.out.println(answer);
    
                    for (int i = 0; i < wordsArray.length(); i++) {
                        String userObject = wordsArray.getString(i);
                        words.add(userObject);
                    }
                    game = new KorakPoKorak(answer,words);
                    round = data.getInt("roundNumber");
    
                    runOnUiThread(new Runnable() {
    
                        @Override
                        public void run() {
    
                            timer.setText("70");
                            editTextInput.setEnabled(true);
                            confirmButton.setEnabled(true);
                        }
                    });
    
    
                    answered = false;
                    buttonAnswer = false;
                    gameTime = 70;
                    handler.postDelayed(runnableGame, 1000);
                    handler.postDelayed(runnable, 10000);
    
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
            }
        };
    
    
    }
    
