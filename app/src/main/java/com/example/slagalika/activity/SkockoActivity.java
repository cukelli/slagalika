package com.example.slagalika.activity;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.slagalika.R;
import com.example.slagalika.adapter.SkockoAdapter;
import com.example.slagalika.adapter.SpojniceAdapter;
import com.example.slagalika.firebase.FirebaseManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SkockoActivity extends AppCompatActivity {

    private ListView placeholderCombination1;
    private ListView placeholderCombination2;
    private ListView placeholderCombination3;
    private ListView placeholderCombination4;
    private ListView placeholderCombination5;
    private ListView placeholderCombination6;
    private SkockoAdapter skockoAdapter;

    private Integer guestScore = 0;


    FirebaseManager firebaseManager = new FirebaseManager();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String variableValue = getIntent().getStringExtra("sessionId");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skocko);
        TextView user1Points = findViewById(R.id.player1Score);

        ValueEventListener vl = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot childSnapshot : snapshot.getChildren()) {
                        int user1 = childSnapshot.child("usernames")
                                .child(getIntent().getStringExtra("username")).
                                getValue(Integer.class);
                        System.out.println(childSnapshot.child("usernames")
                                .child(getIntent().getStringExtra("username")).getKey());
                        user1Points.setText(String.valueOf(user1));
                        guestScore = user1;
                    }
                } else {
                    Log.e(TAG, "Failed to retrieve game session data");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Handle onCancelled event
            }
        };;

        firebaseManager.getUsersPoints(getIntent().getStringExtra("sessionId"), vl);

        ImageButton diamondPng = (ImageButton) findViewById(R.id.diamondPng);
        ImageButton circlePng = (ImageButton) findViewById(R.id.circlePng);
        ImageButton jumperPng = (ImageButton) findViewById(R.id.jumperPng); //skocko
        ImageButton heartPng = (ImageButton) findViewById(R.id.heartPng);
        ImageButton starPng = (ImageButton) findViewById(R.id.starPng);
        ImageButton trianglePng = (ImageButton) findViewById(R.id.trianglePng);

        ArrayList<ImageButton> listForGenerating = new ArrayList<ImageButton>(Arrays.asList(diamondPng, circlePng,
                jumperPng, heartPng, starPng, trianglePng));

        placeholderCombination1 = findViewById(R.id.listViewSkocko1);
        placeholderCombination2 = findViewById(R.id.listViewSkocko2);
        placeholderCombination3 = findViewById(R.id.listViewSkocko3);
        placeholderCombination4 = findViewById(R.id.listViewSkocko4);
        placeholderCombination5 = findViewById(R.id.listViewSkocko5);
        placeholderCombination6 = findViewById(R.id.listViewSkocko6);
        skockoAdapter = new SkockoAdapter(this);
        placeholderCombination1.setAdapter(skockoAdapter);
        placeholderCombination2.setAdapter(skockoAdapter);
        placeholderCombination3.setAdapter(skockoAdapter);
        placeholderCombination4.setAdapter(skockoAdapter);
        placeholderCombination5.setAdapter(skockoAdapter);
        placeholderCombination6.setAdapter(skockoAdapter);


        Button nextGame = (Button) findViewById(R.id.nextGameButton);
        nextGame.setOnClickListener(view -> {
            Intent intent = new Intent(SkockoActivity.this,
                    KorakPoKorakActivity.class);

            intent.putExtra("sessionId", getIntent().getStringExtra("sessionId"));
            intent.putExtra("username", getIntent().getStringExtra("username"));
//            firebaseManager.updateGameSessionPoints(getIntent().getStringExtra("sessionId")
//                    , getIntent().getStringExtra("username"), guestScore);
            startActivity(intent);
        });

    }
        public static ArrayList<ImageButton>
        generateRandomElements(ArrayList<ImageButton> elements, int numElements) {

        ArrayList<ImageButton> randomElements = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < numElements; i++) {
            int randomIndex = random.nextInt(elements.size());
            randomElements.add(elements.get(randomIndex));
        }

        return randomElements;
    }
}