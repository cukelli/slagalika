package com.example.slagalika.activity;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.slagalika.R;
import com.example.slagalika.databinding.ActivityAsocijacijeBinding;
import com.example.slagalika.firebase.FirebaseManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsocijacijeActivity extends AppCompatActivity {

    FirebaseManager firebaseManager;
    DatabaseReference databaseRef;

    private TextView guestScoreNumber;
    EditText finalEditText;
    EditText dEditText;
    EditText cEditText;
    EditText bEditText;
    EditText aEditText;

    List<String> aList;
    List<String> bList;
    List<String> dList;
    List<String> cList;

    List<Button> aButtonList;
    List<Button> bButtonList;
    List<Button> cButtonList;
    List<Button> dButtonList;
    Map<String, String> finalMap;
    int guestScore;
    int guestScore1 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asocijacije);

        firebaseManager = new FirebaseManager();
        databaseRef = FirebaseDatabase.getInstance().getReference().child("asocijacije");

        finalEditText = findViewById(R.id.finalEditText);
        dEditText = findViewById(R.id.dEditText);
        cEditText = findViewById(R.id.cEditText);
        bEditText = findViewById(R.id.bEditText);
        aEditText = findViewById(R.id.aEditText);

        aList = new ArrayList<>();
        bList = new ArrayList<>();
        dList = new ArrayList<>();
        cList = new ArrayList<>();
        finalMap = new HashMap<>();
        guestScore = 0;

        Button submitButton = findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitValues();
            }
        });

        TextView user1Points = findViewById(R.id.player1Score);
        readDataAndSetButtonText();
        finalColumnsLogic();

        Button nextGame = findViewById(R.id.buttonToKorakPoKorak);
        nextGame.setOnClickListener(view -> {
            Intent intent = new Intent(AsocijacijeActivity.this, SkockoActivity.class);
            intent.putExtra("sessionId", getIntent().getStringExtra("sessionId"));
            intent.putExtra("username", getIntent().getStringExtra("username"));
//            firebaseManager.updateGameSessionPoints(getIntent().getStringExtra("sessionId"),
//                    getIntent().getStringExtra("username"), guestScore);
            startActivity(intent);
        });

        ValueEventListener vl = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot childSnapshot : snapshot.getChildren()) {
                        Integer user1 = childSnapshot.child("usernames")
                                .child(getIntent().getStringExtra("username"))
                                .getValue(Integer.class);
                        user1Points.setText(String.valueOf(user1));
                        guestScore1 = user1;
                    }
                } else {
                    Log.e(TAG, "Failed to retrieve game session data");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Handle onCancelled event
            }
        };

        firebaseManager.getUsersPoints(getIntent().getStringExtra("sessionId"), vl);

        ViewGroup viewGroup = (ViewGroup) findViewById(android.R.id.content).getRootView();
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View child = viewGroup.getChildAt(i);
            if (child instanceof TextView) {
                child.setOnClickListener(view -> {
                    ((TextView) child).setTextColor(getResources().getColor(R.color.white));
                });
            }
        }
    }

    private void readDataAndSetButtonText() {
        DatabaseReference gamesRef = FirebaseDatabase.getInstance().getReference()
                .child("games").child("asocijacije");

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    DataSnapshot ASnapshot = dataSnapshot.child("A");
                    DataSnapshot BSnapshot = dataSnapshot.child("B");
                    DataSnapshot CSnapshot = dataSnapshot.child("C");
                    DataSnapshot DSnapshot = dataSnapshot.child("D");

                    for (DataSnapshot childSnapshot : ASnapshot.getChildren()) {
                        String value = childSnapshot.getValue(String.class);
                        aList.add(value);
                    }

                    for (DataSnapshot childSnapshot : BSnapshot.getChildren()) {
                        String value = childSnapshot.getValue(String.class);
                        bList.add(value);
                    }

                    for (DataSnapshot childSnapshot : CSnapshot.getChildren()) {
                        String val = childSnapshot.getValue(String.class);
                        cList.add(val);
                    }

                    for (DataSnapshot childSnapshot : DSnapshot.getChildren()) {
                        String val = childSnapshot.getValue(String.class);
                        dList.add(val);
                    }

                    Button button1 = findViewById(R.id.button5);
                    Button button2 = findViewById(R.id.button9);
                    Button button3 = findViewById(R.id.button10);
                    Button button4 = findViewById(R.id.button11);
                    aButtonList = Arrays.asList(button1,button2,button3,button4);
                    Button button5 = findViewById(R.id.button12);
                    Button button6 = findViewById(R.id.button13);
                    Button button7 = findViewById(R.id.button14);
                    Button button8 = findViewById(R.id.button15);
                    bButtonList = Arrays.asList(button5,button6,button7,button8);
                    Button button9 = findViewById(R.id.button21);
                    Button button10 = findViewById(R.id.button18);
                    Button button11 = findViewById(R.id.button19);
                    Button button12 = findViewById(R.id.button20);
                    cButtonList = Arrays.asList(button9,button10,button11,button12);
                    Button button13 = findViewById(R.id.button17);
                    Button button14 = findViewById(R.id.button22);
                    Button button15 = findViewById(R.id.button23);
                    Button button16 = findViewById(R.id.button24);
                    dButtonList = Arrays.asList(button13,button14,button15,button16);

                    if (aList.size() >= 4) {
                        button1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button1.setText(aList.get(0));
                            }
                        });
                        button2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button2.setText(aList.get(1));
                            }
                        });
                        button3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button3.setText(aList.get(2));
                            }
                        });
                        button4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button4.setText(aList.get(3));
                            }
                        });

                    }
                    if (bList.size() >= 4) {
                        button5.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button5.setText(bList.get(0));
                            }
                        });
                        button6.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button6.setText(bList.get(1));
                            }
                        });
                        button7.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button7.setText(bList.get(2));
                            }
                        });
                        button8.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button8.setText(bList.get(3));
                            }
                        });
                    }

                    if (cList.size() >= 4) {
                        button9.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button9.setText(cList.get(0));
                            }
                        });
                        button10.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button10.setText(cList.get(1));
                            }
                        });
                        button11.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button11.setText(cList.get(2));
                            }
                        });
                        button12.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button12.setText(cList.get(3));
                            }
                        });
                    }

                    if (dList.size() >= 4) {
                        button13.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button13.setText(dList.get(0));
                            }
                        });
                        button14.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button14.setText(dList.get(1));
                            }
                        });
                        button15.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button15.setText(dList.get(2));
                            }
                        });
                        button16.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                button16.setText(dList.get(3));
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle onCancelled event
            }
        };

        gamesRef.addListenerForSingleValueEvent(valueEventListener);
    }


    private void finalColumnsLogic() {
        aEditText.setOnEditorActionListener(getEditorActionListener());
        bEditText.setOnEditorActionListener(getEditorActionListener());
        cEditText.setOnEditorActionListener(getEditorActionListener());
        dEditText.setOnEditorActionListener(getEditorActionListener());
        finalEditText.setOnEditorActionListener(getEditorActionListener());
    }

    private TextView.OnEditorActionListener getEditorActionListener() {
        return (v, actionId, event) -> {
            if ((actionId == EditorInfo.IME_ACTION_DONE || (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER))) {
                submitValues();
                return true;
            }
            return false;
        };
    }

    private void submitValues() {
        guestScore = 0;
        String enteredValueA = aEditText.getText().toString();
        String enteredValueB = bEditText.getText().toString();
        String enteredValueC = cEditText.getText().toString();
        String enteredValueD = dEditText.getText().toString();
        String enteredValueF = finalEditText.getText().toString();

        boolean anyEditTextChanged = false;

        if (aList.size() > 4 && enteredValueA.equals(aList.get(4))) {
            aEditText.setText(aList.get(4));
            aEditText.setEnabled(false);
            aEditText.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
            anyEditTextChanged = true;
            guestScore=6;
            for (Button button : aButtonList){
                String buttonText = button.getText().toString();
                if(buttonText.length()>2){
                    guestScore-=1;
                }
            }


        }

        if (bList.size() > 4 && enteredValueB.equals(bList.get(4))) {
            bEditText.setText(bList.get(4));
            bEditText.setEnabled(false);
            bEditText.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
            anyEditTextChanged = true;
            guestScore=6;
            for (Button button : bButtonList){
                String buttonText = button.getText().toString();
                if(buttonText.length()>2){
                    guestScore-=1;
                }
            }
        }

        if (cList.size() > 4 && enteredValueC.equals(cList.get(4))) {
            cEditText.setText(cList.get(4));
            cEditText.setEnabled(false);
            cEditText.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
            anyEditTextChanged = true;
            guestScore=6;
            for (Button button : cButtonList){
                String buttonText = button.getText().toString();
                if(buttonText.length()>2){
                    guestScore-=1;
                }
            }
        }

        if (dList.size() > 4 && enteredValueD.equals(dList.get(4))) {
            dEditText.setText(dList.get(4));
            dEditText.setEnabled(false);
            dEditText.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
            anyEditTextChanged = true;
            guestScore=6;
            for (Button button : dButtonList){
                String buttonText = button.getText().toString();
                if(buttonText.length()>2){
                    guestScore-=1;
                }
            }
        }

        if (enteredValueF.equals("Nikola Jokic")) {
            finalEditText.setText("Nikola Jokic");
            finalEditText.setEnabled(false);
            finalEditText.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
            anyEditTextChanged = true;
            guestScore+=7;
        }
        guestScore1 += guestScore;
        guestScoreNumber =  findViewById(R.id.player1Score);
        guestScoreNumber.setText(Integer.toString(guestScore1));

        if (!anyEditTextChanged) {
            // Handle case when none of the EditText values match the desired values
        }
    }


    // ...
}
