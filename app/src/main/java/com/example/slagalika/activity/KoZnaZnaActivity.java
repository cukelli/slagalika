package com.example.slagalika.activity;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.slagalika.R;
import com.example.slagalika.firebase.FirebaseManager;
import com.example.slagalika.model.GameSession;
import com.example.slagalika.model.KoZnaZna;
import com.example.slagalika.model.MojBroj;
import com.example.slagalika.model.QuestionAndAnswer;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class KoZnaZnaActivity extends AppCompatActivity {

    //private KoZnaZna game;
    private ArrayList<QuestionAndAnswer> questionsFirebase;
    private DatabaseReference questionRef;
    private TextView questionTextView;
    private Button[] buttons;
    private int currentQuestionIndex = 0;
    private Handler handler = new Handler();
    private Runnable updateButtonsRunnable;
    private int buttonChangeCount = 0;
    private TextView score1TextView;
    private TextView score2TextView;
    private TextView timer;
    private TextView guestScoreNumber;
    private Integer guestScore = 0;
    private FirebaseManager firebaseManager;
    private Socket socket;
    FirebaseAuth auth;
    FirebaseUser user;

    GameSession game;

    ArrayList indexList;

    Socket currentSocket;

    private TextView player1Username;

    private TextView player2Username;

    boolean skip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        socket = null;
        try {
            socket = IO.socket("http://192.168.43.207:3000");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        currentSocket = socket.connect();


       // game = new KoZnaZna(0, 0, questionsFirebase);
        String gameObject = getIntent().getStringExtra("game");
        String indexObject = getIntent().getStringExtra("indexes");
        Gson gson = new Gson();
        game = gson.fromJson(gameObject, GameSession.class);
        indexList = gson.fromJson(indexObject,ArrayList.class);
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();


        super.onCreate(savedInstanceState);



        firebaseManager = new FirebaseManager();

        setContentView(R.layout.activity_ko_zna_zna);
        questionsFirebase = new ArrayList<QuestionAndAnswer>();
        readData();

        questionTextView = findViewById(R.id.textViewQuestion);
        timer = findViewById(R.id.textViewTimer);
        buttons = new Button[4];
        buttons[0] = findViewById(R.id.buttonAnswer1);
        buttons[1] = findViewById(R.id.buttonAnswer2);
        buttons[2] = findViewById(R.id.buttonAnswer3);
        buttons[3] = findViewById(R.id.buttonAnswer4);



        for (Map.Entry<String,Integer> entry : game.getUsernames().entrySet()) {

            int index = indexList.indexOf(entry.getKey());
            System.out.println(entry.getKey());
            if (index == 0){
                player1Username = findViewById(R.id.player1Username);

                player1Username.setText(entry.getKey());
            }
            else {
                player2Username = findViewById(R.id.player2Username);

                player2Username.setText(entry.getKey());
            }
        }
        for (int i = 0; i < buttons.length; i++) {
            int finalI = i;
            buttons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int selectedAnswerIndex = finalI;
                    // Get the current question
                    QuestionAndAnswer currentQuestion = questionsFirebase.get(currentQuestionIndex);

                    // Check if the selected answer is correct
                    boolean isAnswerCorrect = currentQuestion.isAnswerCorrect(selectedAnswerIndex);

                    JSONObject data = new JSONObject();
                    try {
                        int index = currentQuestionIndex - 1;
                        if (currentQuestionIndex == 0) {
                            index = 4;
                        }

                        data.put("username", getIntent().getStringExtra("username"));
                        data.put("answer", isAnswerCorrect);
                        data.put("questionNumber", index);
                        data.put("sessionId", game.getId());
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                    socket.emit("koznazna", data);

                    // Change the button color based on the correctness of the answer
                    if (isAnswerCorrect) {
//                        guestScore+=10;
                        buttons[finalI].setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));

                    } else {
                        buttons[finalI].setBackgroundTintList(ColorStateList.valueOf(Color.RED));
//                        guestScore-=5;
                        // Decrement player's score by 5 if the answer is incorrect
                    }
//                    guestScoreNumber =  findViewById(R.id.player1Score);
//                    guestScoreNumber.setText(Integer.toString(guestScore));

                    for (Button button : buttons) {
                        button.setEnabled(false);
                    }

                }
            });
        }
         skip = true;
        updateButtonsRunnable = new Runnable() {
            private int countdown = 5; // Countdown for the timer

            @Override
            public void run() {
                timer.setText(String.valueOf(countdown)); // Update the timer display

                if (countdown == 1) {
                    countdown = 5; // Reset the countdown to 5 for the next question

                    for (Button button : buttons) {
                        button.setVisibility(View.INVISIBLE);
                        button.setEnabled(false);
                    }
                    if (!skip){
                        JSONObject data = new JSONObject();
                        try {
                            int index = currentQuestionIndex-1;
                            if (currentQuestionIndex == 0){
                                index = 4;
                            }
                            System.out.println(index);
                            data.put("questionNumber", index);
                            data.put("sessionId", game.getId());
                            int[] points = new int[2];

                            for (Map.Entry<String,Integer> entry : game.getUsernames().entrySet()) {
                                int numberPoints = entry.getValue();

                                points[indexList.indexOf(entry.getKey())] = numberPoints;

                            }
                            JSONObject insideObject = new JSONObject();
                            insideObject.put(String.valueOf(0),points[0]);
                            insideObject.put(String.valueOf(1),points[1]);
                            data.put("pointsList",insideObject);
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                        socket.emit("koznaznapoints", data);
                    }
                    updateButtonContent();

                    for (Button button : buttons) {
                        button.setVisibility(View.VISIBLE);
                        button.setEnabled(true);
                        button.setBackgroundTintList(ColorStateList.valueOf
                                (Color.parseColor("#A020F0")));
                    }
                    skip = false;
                } else {
                    countdown--; // Decrement the countdown
                }

                if (countdown == 0 && currentQuestionIndex == 0) {


                    handler.removeCallbacks(updateButtonsRunnable); // Stop the timer
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    // Start the new activity here
                    Intent intent = new Intent(KoZnaZnaActivity.this,
                            KorakPoKorakActivity.class);
                    GameSession gameSend = new GameSession(game.getId(),game.getUsernames());

                    Gson gson = new Gson();
                    String gameObject = gson.toJson(gameSend);
                    intent.putExtra("game", gameObject);

                    gson = new Gson();
                    String indexListSend = gson.toJson(indexList);
                    intent.putExtra("indexes", indexListSend);
                    intent.putExtra("username",  getIntent().getStringExtra("username"));
//                    intent.putExtra("sessionId", getIntent().getStringExtra("sessionId"));
//                    intent.putExtra("username", getIntent().getStringExtra("username"));
                    firebaseManager.updateGameSessionPoints(game.getId(), gameSend.getUsernames());
                    startActivity(intent);

                    return; // Exit the method to prevent further updates
                }

                handler.postDelayed(this, 1000); // Schedule the next update after 1 second
            }
        };


        handler.postDelayed(updateButtonsRunnable, 5000);
        socket.on("pointsUpdate", pointsUpdate);

    }

    private void readData() {
        questionRef = FirebaseDatabase.getInstance().getReference
                ("games/koznazna/questionAnswers");
        questionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
//                    Toast.makeText(KoZnaZnaActivity.this, "Successfully Read",
//                            Toast.LENGTH_SHORT).show();
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        String question = String.valueOf(childSnapshot.child("question")
                                .getValue());
                        int correctAnswerIndex = childSnapshot.child("correctAnswerIndex").
                                getValue(Integer.class);
                        int id = childSnapshot.child("id").getValue(Integer.class);
                        List<String> options = new ArrayList<>();
                        DataSnapshot optionsSnapshot = childSnapshot.child("options");
                        for (DataSnapshot optionSnapshot : optionsSnapshot.getChildren()) {
                            String option = optionSnapshot.getValue(String.class);
                            options.add(option);
                            //System.out.println(option);
                        }
                        QuestionAndAnswer questionAnswer = new QuestionAndAnswer
                                (id, question, options, correctAnswerIndex);
                        questionsFirebase.add(questionAnswer);

                    }
                } else {
//                    Toast.makeText(KoZnaZnaActivity.this, "Data Doesn't Exist",
//                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
//                Toast.makeText(KoZnaZnaActivity.this, "Failed to read: "
//                        + databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateButtonContent() {
        buttonChangeCount++;
        if (buttonChangeCount == 6) {
            handler.removeCallbacks(updateButtonsRunnable); // Stop the timer

            // Start the new activity here

            Intent intent = new Intent(KoZnaZnaActivity.this, KorakPoKorakActivity.class);

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            GameSession gameSend = new GameSession(game.getId(),game.getUsernames());

            Gson gson = new Gson();
            String gameObject = gson.toJson(gameSend);
            intent.putExtra("game", gameObject);

            gson = new Gson();
            String indexListSend = gson.toJson(indexList);
            intent.putExtra("indexes", indexListSend);
            intent.putExtra("username",  getIntent().getStringExtra("username"));
            System.out.println(getIntent().getStringExtra("username") + " print");
//                    intent.putExtra("sessionId", getIntent().getStringExtra("sessionId"));
//                    intent.putExtra("username", getIntent().getStringExtra("username"));
            firebaseManager.updateGameSessionPoints(game.getId(), gameSend.getUsernames());
            startActivity(intent);

            return; // Exit the method to prevent further updates
        }
        QuestionAndAnswer question = questionsFirebase.get(currentQuestionIndex);
        questionTextView.setText(question.getQuestion());
        ArrayList<String> options = new ArrayList<>(question.getOptions());
        int counter = 0;

        for (String option : options) {
            buttons[counter].setText(option);
            counter++;
        }
        currentQuestionIndex = (currentQuestionIndex + 1) % questionsFirebase.size();

    }


    private Emitter.Listener pointsUpdate = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            System.out.println("points update");
            JSONObject data = (JSONObject) args[0];
            try {
                int user0Points = data.getInt("0");
                int user1Points = data.getInt("1");
                game.getUsernames().put((String) indexList.get(0),user0Points);
                game.getUsernames().put((String) indexList.get(1),user1Points);

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        score1TextView = findViewById(R.id.player1Score);
                        score2TextView = findViewById(R.id.player2Score);
                        score1TextView.setText(String.valueOf(user0Points));
                        score2TextView.setText(String.valueOf(user1Points));


                    }
                });




            } catch (JSONException e) {
                throw new RuntimeException(e);
            }



        }
    };

}

