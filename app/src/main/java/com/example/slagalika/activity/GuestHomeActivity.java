package com.example.slagalika.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.example.slagalika.R;
import com.example.slagalika.firebase.FirebaseManager;
import com.example.slagalika.model.GameSession;

import java.util.UUID;

public class GuestHomeActivity extends AppCompatActivity {

    FirebaseManager firebaseManager = new FirebaseManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_home);

        Button guestPlayBtn = (Button) findViewById(R.id.gPlayBtn);
        String username = "guest"+ UUID.randomUUID().toString();
        guestPlayBtn.setOnClickListener(view -> {
            GameSession gameSession = firebaseManager.
                    createGameSession(username,"computer");
            Intent intent = new Intent(GuestHomeActivity.this,KoZnaZnaActivity.class);
            intent.putExtra("sessionId",gameSession.getId());
            intent.putExtra("username",username);
            startActivity(intent);
        });

    }
}