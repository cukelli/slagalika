package com.example.slagalika.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ListView;
import com.example.slagalika.R;
import com.example.slagalika.adapter.CustomBaseAdapterOnlineFriends;
import com.example.slagalika.databinding.ActivityOnlineFriendsBinding;
import com.example.slagalika.databinding.ActivityRegisteredUserHomePageBinding;
import com.example.slagalika.databinding.ActivityUserProfileBinding;
import com.example.slagalika.model.RegisteredUser;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ActivityOnlineFriends extends AppCompatActivity {

    FirebaseAuth auth;
    Button button;
    FirebaseUser user;
    private ActivityOnlineFriendsBinding binding;



//    RegisteredUser[] usersList = {
//            new RegisteredUser("email1@example.com", 3, 10, null, "User 1",
//                    "userprofilefirst"),
//            new RegisteredUser("email1@example.com", 3, 10, null, "User 2",
//                    "userprofilesecond"),
//            new RegisteredUser("email1@example.com", 3, 10, null,
//                    "User 3", "userprofilethird"),
//            new RegisteredUser("email1@example.com", 3, 10, null,
//                    "User 4", "userprofilefourth"),
//            new RegisteredUser("email1@example.com", 3, 10, null,
//
//                    "User 5", "userprofilefifth"),
//
//    };
//
//    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityOnlineFriendsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setItemIconTintList(null);
        navView.setSelectedItemId(R.id.navigation_online_friends);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        if (user == null) {
            Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(intent);
            finish();
        }

        navView.setSelectedItemId(R.id.navigation_online_friends);
        navView.setItemIconTintList(null);

        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.home, R.id.navigation_online_friends)
                .build();
        NavController navController = Navigation.findNavController(this,
                R.id.nav_host_fragment_activity_online_friends);

        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);

        MenuItem profileItem = binding.navView.getMenu().findItem(R.id.navigation_profile);
        MenuItem homeItem = binding.navView.getMenu().findItem(R.id.home);



        profileItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override

            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(ActivityOnlineFriends.this,
                        UserProfileActivity.class);
                startActivity(intent);
                return true;
            }
        });

        homeItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override

            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(ActivityOnlineFriends.this,
                        RegisteredUserHomePageActivity.class);
                startActivity(intent);
                return true;
            }
        });



//        listView = (ListView) findViewById(R.id.listViewOnlineFriends);
//        CustomBaseAdapterOnlineFriends customBaseAdapter =
//                new CustomBaseAdapterOnlineFriends(getApplicationContext(), usersList);
//        listView.setAdapter(customBaseAdapter);
    }
}