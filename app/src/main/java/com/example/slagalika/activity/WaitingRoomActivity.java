package com.example.slagalika.activity;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.slagalika.R;
import com.example.slagalika.firebase.FirebaseManager;
import com.example.slagalika.model.GameSession;
import com.example.slagalika.model.QuestionAndAnswer;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class WaitingRoomActivity extends AppCompatActivity {

    private DatabaseReference testRef;
    private Socket socket;
    String userUsername;

    FirebaseManager firebaseManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_room);

        userUsername = getIntent().getStringExtra("username");
        System.out.println("here");
        // Remove the user from the waiting room



        // Connect to the server
        try {
            socket = IO.socket("http://192.168.43.207:3000");
            socket.connect();
            socket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Exception e = (Exception) args[0];

                    e.printStackTrace();
                   System.out.println("Socket connection error: " + e.getMessage());
                }
            });
            // Send user information to the server on connect
            JSONObject data = new JSONObject();
            data.put("username",userUsername);
            System.out.println(userUsername);
            socket.emit("userInfo", data);

        } catch (URISyntaxException | JSONException e) {
            e.printStackTrace();
            System.out.println("socket error");

        }
        // Handle 'startGame' event from the server
        socket.on("startGame", onStartGame);





    }





    private Emitter.Listener onStartGame = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            Map<String,Integer> usernames = new HashMap<>();
            JSONArray usernamesArray = null;
            try {
                String id = data.getString("id");
                usernamesArray = data.getJSONArray("usernames");
                ArrayList<String> dataList = new ArrayList<>();
                dataList.add("");
                dataList.add("");
                for (int i = 0; i < usernamesArray.length(); i++) {
                    JSONObject userObject = usernamesArray.getJSONObject(i);
                    String username = userObject.getString("username");
                    int points = userObject.getInt("points");
                    usernames.put(username,points);
                    dataList.set(i,username);

                }
                GameSession game = new GameSession(id,usernames);
                Intent intent = new Intent(WaitingRoomActivity.this,
                        KoZnaZnaActivity.class);
                Gson gson = new Gson();
                String gameObject = gson.toJson(game);
                intent.putExtra("game", gameObject);

                gson = new Gson();
                String indexList = gson.toJson(dataList);
                intent.putExtra("indexes", indexList);
                intent.putExtra("username", userUsername);
                startActivity(intent);

            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("pause");
        deleteUserFromFirebase();

    }

    @Override
    public void onStop() {
        super.onStop();
        socket.disconnect();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        socket.disconnect();

    }

    public void deleteUserFromFirebase() {




    }
}