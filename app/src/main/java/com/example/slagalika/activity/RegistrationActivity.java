package com.example.slagalika.activity;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.slagalika.R;
import com.example.slagalika.model.RegisteredUser;
import com.google.android.gms.common.internal.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RegistrationActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
//        FirebaseUser currentUser = mAuth.getCurrentUser();
//        if(currentUser != null){
//            Intent intent = new Intent(getApplicationContext(),RegisteredUserHomePageActivity.class);
//            startActivity(intent);
//            finish();
//        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        mAuth = FirebaseAuth.getInstance();


        Button registerButton = findViewById(R.id.button2);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });
    }

    // ...

    private void registerUser() {
        EditText emailEditText = findViewById(R.id.editTextTextPersonName);
        EditText passwordEditText = findViewById(R.id.editTextTextPassword);
        EditText repeatPasswordEditText = findViewById(R.id.editTextTextPassword2);
        EditText usernameText = findViewById(R.id.editTextTextPersonName2);

        String email = emailEditText.getText().toString().trim();
        String username = usernameText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();
        String repeatPassword = repeatPasswordEditText.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            emailEditText.setError("Email is required.");
            return;
        }

        if (TextUtils.isEmpty(username)) {
            usernameText.setError("Username is required.");
            return;
        }

        if (TextUtils.isEmpty(password)) {
            passwordEditText.setError("Password is required.");
            return;
        }

        if (!password.equals(repeatPassword)) {
            repeatPasswordEditText.setError("Passwords do not match.");
            return;
        }

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("users").orderByChild("username").equalTo(username)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            // User with the given username already exists
                            Toast.makeText(RegistrationActivity.this,
                                    "Username already exists. Please try another username.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Username is available, proceed with user registration
                            mAuth.createUserWithEmailAndPassword(email, password)
                                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()) {
                                                // User creation successful
                                                Toast.makeText(RegistrationActivity.this,
                                                        "Account created.", Toast.LENGTH_SHORT).
                                                        show();
                                                RegisteredUser ru = new RegisteredUser(email,
                                                        0, 0, null, password,
                                                        username);
                                                DatabaseReference databaseReference =
                                                        FirebaseDatabase.getInstance()
                                                                .getReference("/users");
                                                databaseReference.push().setValue(ru)
                                                        .addOnSuccessListener(e -> {
                                                            Intent intent =
                                                                    new Intent
                                                                            (RegistrationActivity.this,
                                                                                    LoginActivity.class);
                                                            startActivity(intent);
                                                            finish();
                                                        }).addOnFailureListener(e ->
                                                                Log.e(TAG,
                                                                        "Error writing data to the database:" +
                                                                                " " + e.getMessage()));
                                            } else {
                                                // If sign in fails, display a message to the user.
                                                Toast.makeText(
                                                        RegistrationActivity.this,
                                                        "User with this email already exists.",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
    }

// ...


}