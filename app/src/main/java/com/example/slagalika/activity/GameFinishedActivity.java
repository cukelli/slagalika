package com.example.slagalika.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.slagalika.R;

public class GameFinishedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_finished);
    }
}