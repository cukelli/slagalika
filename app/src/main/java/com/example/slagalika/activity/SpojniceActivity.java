package com.example.slagalika.activity;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.slagalika.R;
import com.example.slagalika.adapter.SpojniceAdapter;
import com.example.slagalika.enumeration.Field;
import com.example.slagalika.firebase.FirebaseManager;
import com.example.slagalika.model.PairSpojnice;
import com.example.slagalika.model.QuestionAndAnswer;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SpojniceActivity extends AppCompatActivity {

    private ListView listViewPairs;
    private SpojniceAdapter spojniceAdapter;
    private List<String> leftWords;
    private DatabaseReference pairsRef;

    private List<String> rightWords;
    private Integer guestScore = 0;
    private List<PairSpojnice> connectedPairs;
    FirebaseManager firebaseManager = new FirebaseManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String variableValue = getIntent().getStringExtra("sessionId");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spojnice);
        TextView user1Points = findViewById(R.id.player1Score);



        listViewPairs = findViewById(R.id.listViewPairs);
        spojniceAdapter = new SpojniceAdapter(this, new ArrayList<>());
        listViewPairs.setAdapter(spojniceAdapter);

        readData();

        ValueEventListener vl = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot childSnapshot : snapshot.getChildren()) {
//                        int user1 = childSnapshot.child("usernames")
//                                .child(getIntent().getStringExtra("username")).
//                                getValue(Integer.class);
//                        System.out.println(childSnapshot.child("usernames")
//                                .child(getIntent().getStringExtra("username")).getKey());
//                        user1Points.setText(String.valueOf(user1));
//                        guestScore = user1;
                    }
                } else {
                    Log.e(TAG, "Failed to retrieve game session data");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Handle onCancelled event
            }
        };


        Button nextGameBtn = findViewById(R.id.buttonNextGame);
        nextGameBtn.setOnClickListener(v -> {
            Intent intent = new Intent(SpojniceActivity.this, AsocijacijeActivity.class);
//            firebaseManager.updateGameSessionPoints(getIntent().getStringExtra("sessionId"),
//                    getIntent().getStringExtra("username"), guestScore);
            startActivity(intent);
        });


    }

    private void readData() {
        pairsRef = FirebaseDatabase.getInstance().getReference("games/spojnice/rounds/0/pairs");
        pairsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<PairSpojnice> pairsList = new ArrayList<>();  // Create a list to store pairs

                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        String leftWord = String.valueOf(childSnapshot.child
                                ("leftWord").getValue());
                        String rightWord = String.valueOf(childSnapshot.child
                                ("rightWord").getValue());
                        int id = childSnapshot.child("id").getValue(Integer.class);

                        PairSpojnice pair = new PairSpojnice(id, leftWord, rightWord);
                        pairsList.add(pair);  // Add the pair to the list
                    }

                    // Shuffle the pairs
                    Collections.shuffle(pairsList);

                    // Extract the shuffled left and right words
                    List<String> shuffledLeftWords = new ArrayList<>();
                    List<String> shuffledRightWords = new ArrayList<>();
                    List<Integer> shuffledIds = new ArrayList<>();
                    for (PairSpojnice pair : pairsList) {
                        shuffledLeftWords.add(pair.getWord1());
                        shuffledRightWords.add(pair.getWord2());
                        shuffledIds.add(pair.getId());
                    }

                    spojniceAdapter.setPairsList(pairsList);
                } else {
                    Log.e(TAG, "Data Doesn't Exist");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, "Failed to read: " + databaseError.getMessage());
            }
        });
    }


}