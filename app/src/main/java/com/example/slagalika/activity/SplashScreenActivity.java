package com.example.slagalika.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;

import android.os.Bundle;

import com.example.slagalika.R;

public class SplashScreenActivity extends AppCompatActivity {

    Handler h = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        MediaPlayer mediaPlayer = MediaPlayer.create(this,R.raw.start_sound);
        mediaPlayer.start();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashScreenActivity.this,MainActivity.class);
                startActivity(i);
                mediaPlayer.stop();
                mediaPlayer.release();
                finish();

            }
        },6000);


    }
}