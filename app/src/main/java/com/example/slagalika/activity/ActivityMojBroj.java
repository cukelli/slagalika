    package com.example.slagalika.activity;
    import static android.content.ContentValues.TAG;
    import androidx.annotation.NonNull;
    import androidx.appcompat.app.AppCompatActivity;
    
    import android.content.Context;
    import android.content.Intent;
    import android.os.Bundle;
    import android.os.Handler;
    import android.text.InputType;
    import android.util.Log;
    import android.view.View;
    import android.widget.Button;
    import android.widget.EditText;
    import android.widget.ImageButton;
    import android.widget.TextView;
    import android.widget.Toast;
    import com.example.slagalika.R;
    import com.example.slagalika.firebase.FirebaseManager;
    import com.example.slagalika.model.GameSession;
    import com.google.firebase.auth.FirebaseAuth;
    import com.google.firebase.auth.FirebaseUser;
    import com.google.firebase.database.DataSnapshot;
    import com.google.firebase.database.DatabaseError;
    import com.google.firebase.database.ValueEventListener;
    import com.google.gson.Gson;

    import net.objecthunter.exp4j.Expression;
    import net.objecthunter.exp4j.ExpressionBuilder;

    import java.net.URISyntaxException;
    import java.util.ArrayList;
    import java.util.Map;
    import java.util.Random;
    import java.util.Stack;
    import android.hardware.Sensor;
    import android.hardware.SensorEvent;
    import android.hardware.SensorEventListener;
    import android.hardware.SensorManager;
    import android.view.ViewConfiguration;

    import org.json.JSONException;
    import org.json.JSONObject;

    import io.socket.client.IO;
    import io.socket.client.Socket;
    import io.socket.emitter.Emitter;

    public class ActivityMojBroj extends AppCompatActivity {
        FirebaseManager firebaseManager = new FirebaseManager();
        private boolean stopButtonClicked = false;
        private Handler handler = new Handler();
        int counter = 0;
        private Integer guestScore = 0;
        private TextView timer;
        private Stack<Integer> currents = new Stack<>();
        private Stack<Integer> indexes = new Stack<>();
    
        private Runnable timerRunnable;
    
        private int initialTimerCount = 60;
        private int stopButtonTimerCount = 5;
        private boolean gamePlaying = false;

        private Socket socket;
        FirebaseAuth auth;
        FirebaseUser user;

        GameSession game;

        ArrayList indexList;

        Socket currentSocket;

        private TextView player1Username;

        private TextView player2Username;

        private TextView user1Points;

        private TextView user2Points;

        private int roundsPlayed = 0;
        private String loggedUsername;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_moj_broj);


            String gameObject = getIntent().getStringExtra("game");
            String indexObject = getIntent().getStringExtra("indexes");
            Gson gson = new Gson();
            game = gson.fromJson(gameObject, GameSession.class);
            indexList = gson.fromJson(indexObject,ArrayList.class);
            user1Points = findViewById(R.id.player1Score);
            user2Points = findViewById(R.id.player2Score);
            loggedUsername =  getIntent().getStringExtra("username");

            user1Points.setText(String.valueOf(game.getUsernames().get(indexList.get(0))));
            user2Points.setText(String.valueOf(game.getUsernames().get(indexList.get(1))));


            try {
                socket = IO.socket("http://192.168.43.207:3000");
                socket.connect();

                JSONObject data = new JSONObject();
                data.put("username",loggedUsername);
                data.put("sessionId",game.getId());
                socket.emit("mojbrojgame", data);

            } catch (URISyntaxException | JSONException e) {
                e.printStackTrace();
                System.out.println("socket error");

            }
            Button plusButton = findViewById(R.id.plusButton);
            Button minusButton = findViewById(R.id.buttonMinus);
            Button multiplyButton = findViewById(R.id.buttonMultiply);
            Button divideButton = findViewById(R.id.buttonDivide);
            Button leftBracketButton = findViewById(R.id.leftBracketButton);
            Button rightBracketButton = findViewById(R.id.rightBracketButton);
            Button stopButton = findViewById(R.id.stopButton);
            if (!loggedUsername.equalsIgnoreCase(String.valueOf(indexList.get(0)))) {
                stopButton.setEnabled(false);
            }

            for (Map.Entry<String,Integer> entry : game.getUsernames().entrySet()) {

                int index = indexList.indexOf(entry.getKey());
                System.out.println(entry.getKey());
                if (index == 0){
                    player1Username = findViewById(R.id.player1Username);

                    player1Username.setText(entry.getKey());
                }
                else {
                    player2Username = findViewById(R.id.player2Username);

                    player2Username.setText(entry.getKey());
                }
            }

            //timer
            timer = findViewById(R.id.textViewTimer);
    

    
            ImageButton deleteButton = findViewById(R.id.deleteImageButton);
            ImageButton confirmButton = findViewById(R.id.confirmImageButton);
            EditText editTextNumber = findViewById(R.id.editTextNumber);
            editTextNumber.setInputType(InputType.TYPE_NULL);
    
            generateRandomNumber();
    
    
            plusButton.setOnClickListener(view -> {
                editTextNumber.append("+");
                currents.push(1);
                indexes.push(0);
            });
            minusButton.setOnClickListener(view -> {
                editTextNumber.append("-");
                currents.push(1);
                indexes.push(0);
    
    
            });
            multiplyButton.setOnClickListener(view -> {
                editTextNumber.append("*");
                currents.push(1);
                indexes.push(0);
    
            });
            leftBracketButton.setOnClickListener(view -> {
                editTextNumber.append("(");
                currents.push(1);
                indexes.push(0);
    
    
            });
            rightBracketButton.setOnClickListener(view -> {
                editTextNumber.append(")");
                currents.push(1);
                indexes.push(0);
    
            });
            divideButton.setOnClickListener(view -> {
                editTextNumber.append("÷");
                currents.push(1);
                indexes.push(0);
    
    
            });
            deleteButton.setOnClickListener(view -> {
                int length = editTextNumber.getText().length();
    
                if (length > 0) {
                    int pop = currents.pop(); //DUZINA BROJA NA KOJI JE KLIKNUTO
                    int index = indexes.pop(); //INDEXI BUTTON
    
                    editTextNumber.getText().delete(length - pop, length);
                    if (index != 0) {
    
                        if (index == 1) {
                            Button firstSingleDigitNum = findViewById(R.id.buttonFirstSingleDigitNumber);
                            firstSingleDigitNum.setEnabled(true);
                        } else if (index == 2) {
                            Button secondSingleDigitNum = findViewById(R.id.buttonSecondSingleDigitNumber);
                            secondSingleDigitNum.setEnabled(true);
                        } else if (index == 3) {
                            Button thirdSingleDigitNum = findViewById(R.id.buttonThirdSingleDigitNumber);
                            thirdSingleDigitNum.setEnabled(true);
                        } else if (index == 4) {
                            Button fourthSingleDigitNum = findViewById(R.id.buttonFourthSingleDigitNumber);
                            fourthSingleDigitNum.setEnabled(true);
                        } else if (index == 5) {
                            Button specialNum1 = findViewById(R.id.randomNumberButton1);
                            specialNum1.setEnabled(true);
                        } else {
                            Button specialNum2 = findViewById(R.id.randomNumberButton2);
                            specialNum2.setEnabled(true);
    
                        }
                    }
                }
            });
            confirmButton.setOnClickListener(view -> {
                double result =  calculateEquation();
                try {
                    JSONObject data = new JSONObject();
                    data.put("sessionId",game.getId());
                    data.put("username", loggedUsername);
                    data.put("roundNumber", roundsPlayed);
                    data.put("result", result);
                    socket.emit("mojbrojconfirm", data);
                } catch (Exception e){

                }
            });
            stopButton.setOnClickListener(view -> {
                if (counter == 0) {
                    stopButtonClicked = true;
                    generateRandomNumber();

                    counter++;
                } else if (counter == 1) {
                    generateNumbersOnButons();
                    stopButton.setEnabled(false);
                    counter++;
    
                }

            });
    
    
            timerRunnable = new Runnable() {
                @Override
                public void run() {
                    if (!gamePlaying) {
                        if ((stopButtonTimerCount == 0) || (stopButtonClicked && counter == 2)) {
                            stopButtonTimerCount = 60;
                            gamePlaying = true;
                            roundsPlayed ++;
                            timer.setText(String.valueOf(60));
                            handler.postDelayed(this, 1000);
    
                        }
                        if ((!stopButtonClicked && counter != 1) || (stopButtonClicked && counter == 1)) {
                            timer.setText(String.valueOf(stopButtonTimerCount--));
                            handler.postDelayed(this, 1000);
                            //za 5 sekundi pre igre
    
                        }
                    } else {
                        if (stopButtonTimerCount != 0) {
                            timer.setText(String.valueOf(stopButtonTimerCount--));
                            handler.postDelayed(this, 1000);
                        } else {
                            try {
                                double result = calculateEquation();
                                JSONObject data = new JSONObject();
                                data.put("sessionId", game.getId());
                                data.put("username", loggedUsername);
                                data.put("result", result);
                                data.put("roundNumber", roundsPlayed);
                                socket.emit("mojbrojconfirm", data);
                            } catch (Exception e) {

                            }

                        }

                    }
                }
            };
            handler.postDelayed(timerRunnable, 0);
            handler.postDelayed(() -> {
                if (!stopButtonClicked && counter == 0) {
                    stopButtonClicked = true;
                    stopButton.setEnabled(false);
                    counter = 2;
                    generateRandomNumber();
                    generateNumbersOnButons();
                }
                if (stopButtonClicked && counter == 1) {
                    stopButton.setEnabled(false);
                    counter = 2;
                    generateNumbersOnButons();
                }
    
            }, 5000 );

            ValueEventListener vl = new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {
                        for (DataSnapshot childSnapshot : snapshot.getChildren()) {
                            int user1 = childSnapshot.child("usernames").
                                    child(getIntent().getStringExtra("username")).
                                    getValue(Integer.class);
                            System.out.println(childSnapshot.child("usernames").
                                    child(getIntent().getStringExtra("username")).getKey());
                            user1Points.setText(String.valueOf(user1));
                            guestScore = user1;
                        }
                    } else {
                        Log.e(TAG, "Failed to retrieve game session data");
                    }
                }
    
                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    // Handle onCancelled event
                }
            };
    
//            firebaseManager.getUsersPoints(getIntent().getStringExtra("sessionId"), vl);


            socket.on("mojbrojupdate", numberUpdate);
            socket.on("mojbrojupdatecalculating", nubmersCalcUpdate);
            socket.on("mojbrojconfirmupdate", confirmUpdate);
            socket.on("pointsUpdate", pointsUpdate);

        }

        private Emitter.Listener numberUpdate = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try {
                    int numberU = data.getInt("number");
                    System.out.println(numberU);
                    stopButtonClicked = true;
                    counter = 1;
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            TextView number = findViewById(R.id.number);
                            number.setText(String.valueOf(numberU));
                        }
                    });

                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }

            }
        };

        private Emitter.Listener nubmersCalcUpdate = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try {
                    int firstNumber = data.getInt("firstNumber");
                    int secondNumber = data.getInt("secondNumber");
                    int thirdNumber = data.getInt("thirdNumber");
                    int fourthNumber = data.getInt("fourthNumber");
                    int specialNumber1 = data.getInt("specialNumber1");
                    int specialNumber2 = data.getInt("specialNumber2");
                    counter = 2;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            EditText editTextNumber = findViewById(R.id.editTextNumber);

                            Button firstSingleDigitNum = findViewById(R.id.buttonFirstSingleDigitNumber);
                            firstSingleDigitNum.setEnabled(true);
                            firstSingleDigitNum.setText(String.valueOf(firstNumber));
                            firstSingleDigitNum.setOnClickListener(view -> {
                                if (indexes.size() != 0) {
                                    int index = indexes.pop();
                                    if (index != 0) {
                                        indexes.push(index);
                                        Toast.makeText(ActivityMojBroj.this,
                                                "Enter sign before another number"
                                                , Toast.LENGTH_SHORT).show();
                                    } else {
                                        indexes.push(index);
                                        editTextNumber.append(String.valueOf(firstNumber));
                                        currents.push(1);
                                        indexes.push(1);
                                        firstSingleDigitNum.setEnabled(false);
                                    }
                                } else {
                                    editTextNumber.append(String.valueOf(firstNumber));
                                    currents.push(1);
                                    indexes.push(1);
                                    firstSingleDigitNum.setEnabled(false);
                                }
                            });

                            Button secondSingleDigitNum = findViewById(R.id.buttonSecondSingleDigitNumber);
                            secondSingleDigitNum.setEnabled(true);
                            secondSingleDigitNum.setText(String.valueOf(secondNumber));
                            secondSingleDigitNum.setOnClickListener(view -> {
                                if (indexes.size() != 0) {
                                    int index = indexes.pop();
                                    if (index != 0) {
                                        indexes.push(index);
                                        Toast.makeText(ActivityMojBroj.this, "Enter sign before another number"
                                                , Toast.LENGTH_SHORT).show();
                                    } else {
                                        indexes.push(index);
                                        editTextNumber.append(String.valueOf(secondNumber));
                                        currents.push(1);
                                        indexes.push(2);
                                        secondSingleDigitNum.setEnabled(false);
                                    }
                                } else {
                                    editTextNumber.append(String.valueOf(secondNumber));
                                    currents.push(1);
                                    indexes.push(2);
                                    secondSingleDigitNum.setEnabled(false);
                                }

                            });
                            Button thirdSingleDigitNum = findViewById(R.id.buttonThirdSingleDigitNumber);
                            thirdSingleDigitNum.setEnabled(true);
                            thirdSingleDigitNum.setText(String.valueOf(thirdNumber));
                            thirdSingleDigitNum.setOnClickListener(view -> {
                                if (indexes.size() != 0) {

                                    int index = indexes.pop();
                                    if (index != 0) {
                                        indexes.push(index);
                                        Toast.makeText(ActivityMojBroj.this, "Enter sign before another number"
                                                , Toast.LENGTH_SHORT).show();
                                    } else {
                                        indexes.push(index);
                                        editTextNumber.append(String.valueOf(thirdNumber));
                                        currents.push(1);
                                        indexes.push(3);
                                        thirdSingleDigitNum.setEnabled(false);


                                    }
                                } else {
                                    editTextNumber.append(String.valueOf(thirdNumber));
                                    currents.push(1);
                                    indexes.push(3);
                                    thirdSingleDigitNum.setEnabled(false);
                                }
                            });
                            Button fourthSingleDigitNum = findViewById(R.id.buttonFourthSingleDigitNumber);
                            fourthSingleDigitNum.setEnabled(true);
                            fourthSingleDigitNum.setText(String.valueOf(fourthNumber));
                            fourthSingleDigitNum.setOnClickListener(view -> {
                                if (indexes.size() != 0) {

                                    int index = indexes.pop();
                                    if (index != 0) {
                                        indexes.push(index);
                                        Toast.makeText(ActivityMojBroj.this, "Enter sign before another number"
                                                , Toast.LENGTH_SHORT).show();
                                    } else {
                                        indexes.push(index);
                                        editTextNumber.append(String.valueOf(fourthNumber));
                                        currents.push(1);
                                        indexes.push(4);
                                        fourthSingleDigitNum.setEnabled(false);
                                    }
                                } else {
                                    editTextNumber.append(String.valueOf(fourthNumber));
                                    currents.push(1);
                                    indexes.push(4);
                                    fourthSingleDigitNum.setEnabled(false);
                                }

                            });
                            Button specialNum1 = findViewById(R.id.randomNumberButton1);
                            specialNum1.setEnabled(true);
                            specialNum1.setText(String.valueOf(specialNumber1));
                            specialNum1.setOnClickListener(view -> {
                                if (indexes.size() != 0) {

                                    int index = indexes.pop();
                                    if (index != 0) {
                                        indexes.push(index);
                                        Toast.makeText(ActivityMojBroj.this, "Enter sign before another number"
                                                , Toast.LENGTH_SHORT).show();
                                    } else {
                                        indexes.push(index);
                                        editTextNumber.append(String.valueOf(specialNumber1));
                                        currents.push(2);
                                        indexes.push(5);
                                        specialNum1.setEnabled(false);
                                    }
                                } else {
                                    editTextNumber.append(String.valueOf(specialNumber1));
                                    currents.push(2);
                                    indexes.push(5);
                                    specialNum1.setEnabled(false);
                                }

                            });
                            Button specialNum2 = findViewById(R.id.randomNumberButton2);
                            specialNum2.setEnabled(true);
                            specialNum2.setText(String.valueOf(specialNumber2));
                            specialNum2.setOnClickListener(view -> {
                                if (indexes.size() != 0) {

                                    int index = indexes.pop();
                                    if (index != 0) {
                                        indexes.push(index);
                                        Toast.makeText(ActivityMojBroj.this, "Enter sign before another number"
                                                , Toast.LENGTH_SHORT).show();
                                    } else {
                                        indexes.push(index);
                                        String currentNumber = String.valueOf(specialNumber2);
                                        editTextNumber.append(currentNumber);
                                        currents.push(currentNumber.length());
                                        indexes.push(6);
                                        specialNum2.setEnabled(false);

                                    }
                                } else {
                                    String currentNumber = String.valueOf(specialNumber2);
                                    editTextNumber.append(currentNumber);
                                    currents.push(currentNumber.length());
                                    indexes.push(6);
                                    specialNum2.setEnabled(false);
                                }
                            });
                        }
                    });

                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }

            }
        };

        private Emitter.Listener confirmUpdate = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            if (roundsPlayed == 1) {
                                handler.removeCallbacks(timerRunnable);
                                currents = new Stack<>();
                                indexes = new Stack<>();
                                initialTimerCount = 60;
                                counter = 0;
                                stopButtonTimerCount = 5;
                                stopButtonClicked = false;
                                gamePlaying = false;

                                Button stopButton = findViewById(R.id.stopButton);
                                if (!loggedUsername.equalsIgnoreCase(String.valueOf
                                        (indexList.get(0)))) {
                                    stopButton.setEnabled(true);
                                }
                                Button firstSingleDigitNum = findViewById(R.id.buttonFirstSingleDigitNumber);
                                firstSingleDigitNum.setText("");
                                Button secondSingleDigitNum = findViewById(R.id.buttonSecondSingleDigitNumber);
                                secondSingleDigitNum.setText("");
                                Button thirdSingleDigitNum = findViewById(R.id.buttonThirdSingleDigitNumber);
                                thirdSingleDigitNum.setText("");
                                Button fourthSingleDigitNum = findViewById(R.id.buttonFourthSingleDigitNumber);
                                fourthSingleDigitNum.setText("");
                                Button specialNum1 = findViewById(R.id.randomNumberButton1);
                                specialNum1.setText("");
                                Button specialNum2 = findViewById(R.id.randomNumberButton2);
                                specialNum2.setText("");
                                TextView number = findViewById(R.id.number);
                                number.setText("");
                                EditText editTextNumber = findViewById(R.id.editTextNumber);
                                editTextNumber.setText("");
                                editTextNumber.setInputType(InputType.TYPE_NULL);
                                generateRandomNumber();
                                handler.postDelayed(timerRunnable, 0);

                                handler.postDelayed(() -> {
                                    if (!stopButtonClicked && counter == 0) {
                                        stopButtonClicked = true;
                                        stopButton.setEnabled(false);
                                        counter = 2;
                                        generateRandomNumber();
                                        generateNumbersOnButons();
                                    }
                                    if (stopButtonClicked && counter == 1) {
                                        stopButton.setEnabled(false);
                                        counter = 2;
                                        generateNumbersOnButons();
                                    }

                                }, 5000);


                            }else {
                                try {
                                    Thread.sleep(200);
                                } catch (InterruptedException e) {
                                    throw new RuntimeException(e);
                                }
                                Intent intent = new Intent(ActivityMojBroj.this,
                                        GameFinishedActivity.class);
                                GameSession gameSend = new GameSession(game.getId(),game.getUsernames());

                                Gson gson = new Gson();
                                String gameObject = gson.toJson(gameSend);
                                intent.putExtra("game", gameObject);

                                gson = new Gson();
                                String indexListSend = gson.toJson(indexList);
                                intent.putExtra("indexes", indexListSend);
                                intent.putExtra("username",  getIntent().getStringExtra("username"));
                                firebaseManager.updateGameSessionPoints(game.getId(), gameSend.getUsernames());
                                startActivity(intent);
                            }

                        }
                    });

                } catch (Exception e) {
                    throw new RuntimeException(e);
                }

            }
        };
        private void generateRandomNumber() {
            Random random = new Random();
            int randomNumber = random.nextInt(1001);
            TextView number = findViewById(R.id.number);
            number.setText(String.valueOf(randomNumber));
    
            if (!stopButtonClicked) {
                Handler handler = new Handler();
                handler.postDelayed(this::generateRandomNumber, 100);
            } else {
                try {
                    JSONObject data = new JSONObject();
                    data.put("sessionId",game.getId());
                    data.put("number", randomNumber);
                    data.put("round", roundsPlayed);
                    socket.emit("mojbrojnumber", data);
                } catch (Exception e){

                }
            }
    
        }
    
        private void generateNumbersOnButons() {
            Random random = new Random();
            EditText editTextNumber = findViewById(R.id.editTextNumber);
    
            int firstNumber = 1 + random.nextInt(9 - 1 + 1);
            int secondNumber = 1 + random.nextInt(9 - 1 + 1);
            int thirdNumber = 1 + random.nextInt(9 - 1 + 1);
            int fourthNumber = 1 + random.nextInt(9 - 1 + 1);
            //10,15,20
            int specialNumber1 = random.nextInt(3) * 5 + 10;
            //25,50,75,100
            int specialNumber2 = random.nextInt(4) * 25 + 25;

            try {
                JSONObject data = new JSONObject();
                data.put("sessionId",game.getId());
                data.put("firstNumber", firstNumber);
                data.put("secondNumber", secondNumber);
                data.put("thirdNumber", thirdNumber);
                data.put("fourthNumber", fourthNumber);
                data.put("specialNumber1", specialNumber1);
                data.put("specialNumber2", specialNumber2);
                socket.emit("mojbrojcalculating", data);
            } catch (Exception e){

            }
            Button firstSingleDigitNum = findViewById(R.id.buttonFirstSingleDigitNumber);
            firstSingleDigitNum.setText(String.valueOf(firstNumber));
            firstSingleDigitNum.setOnClickListener(view -> {
                if (indexes.size() != 0) {
                    int index = indexes.pop();
                    if (index != 0) {
                        indexes.push(index);
                        Toast.makeText(ActivityMojBroj.this,
                                "Enter sign before another number"
                                , Toast.LENGTH_SHORT).show();
                    } else {
                        indexes.push(index);
                        editTextNumber.append(String.valueOf(firstNumber));
                        currents.push(1);
                        indexes.push(1);
                        firstSingleDigitNum.setEnabled(false);
                    }
                } else {
                    editTextNumber.append(String.valueOf(firstNumber));
                    currents.push(1);
                    indexes.push(1);
                    firstSingleDigitNum.setEnabled(false);
                }
            });
    
            Button secondSingleDigitNum = findViewById(R.id.buttonSecondSingleDigitNumber);
            secondSingleDigitNum.setText(String.valueOf(secondNumber));
            secondSingleDigitNum.setOnClickListener(view -> {
                if (indexes.size() != 0) {
                    int index = indexes.pop();
                    if (index != 0) {
                        indexes.push(index);
                        Toast.makeText(ActivityMojBroj.this, "Enter sign before another number"
                                , Toast.LENGTH_SHORT).show();
                    } else {
                        indexes.push(index);
                        editTextNumber.append(String.valueOf(secondNumber));
                        currents.push(1);
                        indexes.push(2);
                        secondSingleDigitNum.setEnabled(false);
                    }
                } else {
                    editTextNumber.append(String.valueOf(secondNumber));
                    currents.push(1);
                    indexes.push(2);
                    secondSingleDigitNum.setEnabled(false);
                }
    
            });
            Button thirdSingleDigitNum = findViewById(R.id.buttonThirdSingleDigitNumber);
            thirdSingleDigitNum.setText(String.valueOf(thirdNumber));
            thirdSingleDigitNum.setOnClickListener(view -> {
                if (indexes.size() != 0) {
    
                    int index = indexes.pop();
                    if (index != 0) {
                        indexes.push(index);
                        Toast.makeText(ActivityMojBroj.this, "Enter sign before another number"
                                , Toast.LENGTH_SHORT).show();
                    } else {
                        indexes.push(index);
                        editTextNumber.append(String.valueOf(thirdNumber));
                        currents.push(1);
                        indexes.push(3);
                        thirdSingleDigitNum.setEnabled(false);
    
    
                    }
                } else {
                    editTextNumber.append(String.valueOf(thirdNumber));
                    currents.push(1);
                    indexes.push(3);
                    thirdSingleDigitNum.setEnabled(false);
                }
            });
            Button fourthSingleDigitNum = findViewById(R.id.buttonFourthSingleDigitNumber);
            fourthSingleDigitNum.setText(String.valueOf(fourthNumber));
            fourthSingleDigitNum.setOnClickListener(view -> {
                if (indexes.size() != 0) {
    
                    int index = indexes.pop();
                    if (index != 0) {
                        indexes.push(index);
                        Toast.makeText(ActivityMojBroj.this, "Enter sign before another number"
                                , Toast.LENGTH_SHORT).show();
                    } else {
                        indexes.push(index);
                        editTextNumber.append(String.valueOf(fourthNumber));
                        currents.push(1);
                        indexes.push(4);
                        fourthSingleDigitNum.setEnabled(false);
                    }
                } else {
                    editTextNumber.append(String.valueOf(fourthNumber));
                    currents.push(1);
                    indexes.push(4);
                    fourthSingleDigitNum.setEnabled(false);
                }
    
            });
            Button specialNum1 = findViewById(R.id.randomNumberButton1);
            specialNum1.setText(String.valueOf(specialNumber1));
            specialNum1.setOnClickListener(view -> {
                if (indexes.size() != 0) {
    
                    int index = indexes.pop();
                    if (index != 0) {
                        indexes.push(index);
                        Toast.makeText(ActivityMojBroj.this, "Enter sign before another number"
                                , Toast.LENGTH_SHORT).show();
                    } else {
                        indexes.push(index);
                        editTextNumber.append(String.valueOf(specialNumber1));
                        currents.push(2);
                        indexes.push(5);
                        specialNum1.setEnabled(false);
                    }
                } else {
                    editTextNumber.append(String.valueOf(specialNumber1));
                    currents.push(2);
                    indexes.push(5);
                    specialNum1.setEnabled(false);
                }
    
            });
            Button specialNum2 = findViewById(R.id.randomNumberButton2);
            specialNum2.setText(String.valueOf(specialNumber2));
            specialNum2.setOnClickListener(view -> {
                if (indexes.size() != 0) {
    
                    int index = indexes.pop();
                    if (index != 0) {
                        indexes.push(index);
                        Toast.makeText(ActivityMojBroj.this, "Enter sign before another number"
                                , Toast.LENGTH_SHORT).show();
                    } else {
                        indexes.push(index);
                        String currentNumber = String.valueOf(specialNumber2);
                        editTextNumber.append(currentNumber);
                        currents.push(currentNumber.length());
                        indexes.push(6);
                        specialNum2.setEnabled(false);
    
                    }
                } else {
                    String currentNumber = String.valueOf(specialNumber2);
                    editTextNumber.append(currentNumber);
                    currents.push(currentNumber.length());
                    indexes.push(6);
                    specialNum2.setEnabled(false);
                }
            });
    
        }
    
    
        public double calculateEquation() {
            EditText editTextNumber = findViewById(R.id.editTextNumber);
            TextView resultTextView = findViewById(R.id.textViewResult);
            String equation = editTextNumber.getText().toString();
            double result;
            try {
                result = evaluateExpression(equation);
                resultTextView.setText("Result: " + result);
    
                return result;
            } catch (Exception e) {
                result = 0;
                return result;
            }
        }
    
        public double evaluateExpression(String expression) throws Exception {
            Expression exp = new ExpressionBuilder(expression).build();
            return exp.evaluate();
        }
    
        private void startGameTimer() {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                }
            }, initialTimerCount * 1000);
        }

        private Emitter.Listener pointsUpdate = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                System.out.println("points update");
                JSONObject data = (JSONObject) args[0];
                try {
                    int user0PointsN = data.getInt("0")+
                            game.getUsernames().get( indexList.get(0));
                    int user1PointsN = data.getInt("1")+
                            game.getUsernames().get( indexList.get(1));

                    game.getUsernames().put((String) indexList.get(0),user0PointsN);
                    game.getUsernames().put((String) indexList.get(1),user1PointsN);

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            user1Points = findViewById(R.id.player1Score);
                            user2Points = findViewById(R.id.player2Score);
                            user1Points.setText(String.valueOf(user0PointsN));
                            user2Points.setText(String.valueOf(user1PointsN));


                        }
                    });




                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }



            }
        };
}
    
    
    
    
