package com.example.slagalika.enumeration;

public enum Field {

    GEOGRAPHY,HISTORY,POLITICS,NATURE,BOOKS,GAMES,MUSIC,MOVIES
}
