package com.example.slagalika.adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.recyclerview.widget.RecyclerView;

import com.example.slagalika.R;
import com.example.slagalika.model.PairSpojnice;

import java.util.ArrayList;
import java.util.List;

public class SkockoAdapter extends ArrayAdapter<Integer> {

    Context context;
    LayoutInflater inflater;

    public SkockoAdapter(Context context) {
        super(context, 0);
        this.context = context;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.activity_skoco_kombinacija_placeholder, parent, false);
        }
        return convertView;
    }

    private static class ViewHolder {
    }





}
