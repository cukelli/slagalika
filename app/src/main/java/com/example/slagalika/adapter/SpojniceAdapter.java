package com.example.slagalika.adapter;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.example.slagalika.R;
import com.example.slagalika.model.PairSpojnice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SpojniceAdapter extends BaseAdapter {

    private Context context;
    private List<PairSpojnice> pairsList;

    public SpojniceAdapter(Context context, List<PairSpojnice> pairsList) {
        this.context = context;
        this.pairsList = pairsList;
    }

    @Override
    public int getCount() {
        return pairsList.size();
    }

    @Override
    public Object getItem(int position) {
        return pairsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.spojnice_pair, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.buttonLeft = convertView.findViewById(R.id.buttonLeftSpojnice);
            viewHolder.buttonRight = convertView.findViewById(R.id.buttonRightSpojnice);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        PairSpojnice pair = pairsList.get(position);
        viewHolder.buttonLeft.setText(pair.getWord1());
        viewHolder.buttonRight.setText(pair.getWord2());
//
//        if (pair.leftWordConnected()) {
//            viewHolder.buttonLeft.setBackgroundColor(Color.GREEN);
//        } else {
//            viewHolder.buttonLeft.setBackgroundColor(Color.BLUE);
//        }
//
//        if (pair.rightWordConnected()) {
//            viewHolder.buttonRight.setBackgroundColor(Color.GREEN);
//        } else {
//            viewHolder.buttonRight.setBackgroundColor(Color.BLUE);
//        }

        viewHolder.buttonLeft.setTag(pair.getId());
        viewHolder.buttonRight.setTag(pair.getId());


        return convertView;
    }

    public void setPairsList(List<PairSpojnice> pairsList) {
        // Shuffle the left words
        List<String> shuffledLeftWords = new ArrayList<>();
        for (PairSpojnice pair : pairsList) {
            shuffledLeftWords.add(pair.getWord1());
        }
        Collections.shuffle(shuffledLeftWords);

        // Shuffle the right words
        List<String> shuffledRightWords = new ArrayList<>();
        for (PairSpojnice pair : pairsList) {
            shuffledRightWords.add(pair.getWord2());
        }
        Collections.shuffle(shuffledRightWords);

        // Update the pairs list with the shuffled words
        List<PairSpojnice> shuffledPairsList = new ArrayList<>();
        int size = Math.min(shuffledLeftWords.size(), shuffledRightWords.size());
        for (int i = 0; i < size; i++) {
            PairSpojnice pair = new PairSpojnice(i, shuffledLeftWords.get(i),
                    shuffledRightWords.get(i));
            shuffledPairsList.add(pair);
        }

        this.pairsList = shuffledPairsList;
        notifyDataSetChanged();
    }


    private static class ViewHolder {
        Button buttonLeft;
        Button buttonRight;
    }
}
