package com.example.slagalika.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.slagalika.R;
import com.example.slagalika.model.RegisteredUser;

import java.util.ArrayList;
import java.util.List;

public class RankingAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<RegisteredUser> mUsers;

    public RankingAdapter(Context context, ArrayList<RegisteredUser> users) {
        mContext = context;
        mUsers = users;
    }

    @Override
    public int getCount() {
        return mUsers.size();
    }

    @Override
    public RegisteredUser getItem(int position) {
        return mUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.ranking_item, parent, false);

            holder = new ViewHolder();
            holder.userName = convertView.findViewById(R.id.user_name);
            holder.userRanking = convertView.findViewById(R.id.user_ranking);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        RegisteredUser user = getItem(position);
        holder.userName.setText(user.getUsername());
        holder.userRanking.setText(Integer.toString(user.getStars()));

        return convertView;
    }

    private static class ViewHolder {
        TextView userName;
        TextView userRanking;
    }
}
