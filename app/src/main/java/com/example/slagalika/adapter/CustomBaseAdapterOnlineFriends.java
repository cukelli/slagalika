package com.example.slagalika.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.slagalika.R;
import com.example.slagalika.model.RegisteredUser;

public class CustomBaseAdapterOnlineFriends extends BaseAdapter {

    Context context;
    RegisteredUser[] friendList;
    LayoutInflater inflater;

    public CustomBaseAdapterOnlineFriends(Context ctx, RegisteredUser[] friendList) {
        this.context = ctx;
        this.friendList = friendList;
        inflater = LayoutInflater.from(ctx);
    }

    @Override
    public int getCount() {
        return friendList.length;
    }

    @Override
    public Object getItem(int position) {
        return friendList[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.activity_custom_list_view_online_friends, null);
        TextView txtView = convertView.findViewById(R.id.textViewOnlineFriend);
        ImageView friendImg = convertView.findViewById(R.id.imageIconOnlineFriend);
        RegisteredUser friend = friendList[position];
        txtView.setText(friend.getUsername());
        int imageResource = context.getResources().getIdentifier(friend.getImagePath(),
                "drawable", context.getPackageName());
        friendImg.setImageResource(imageResource);
        return convertView;
    }
}
