const express = require('express');
const http = require('http');
const socketIO = require('socket.io');
const debug = require('debug')('app:server');
const PORT = 3000;
const app = express();
app.use(express.static('public'));
const { randomUUID } = require('crypto');
const server = http.createServer(app);

const firebaseAdmin = require('firebase-admin');
const serviceAccount = require('./slagalika-f5f4b-firebase-adminsdk-gwzou-62965910e3.json');
// Replace with the actual path to your service account key JSON file.

firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert(serviceAccount),
  databaseURL: 'https://slagalika-f5f4b-default-rtdb.europe-west1.firebasedatabase.app/'
});

server.listen(PORT, '0.0.0.0', () => {
  debug(`Server is running on http://0.0.0.0:${PORT}`);
});
const io = socketIO(server);


const lobby = [];
const readyPlayers = [];
const playingGames = {};
let gameSessionCreated = false; // Flag to track if a game session has been created

io.on('connection', (socket) => {
  debug(`Player connected: ${socket.id}`);
  console.log(socket);

  // Handle user information sent from the client
  socket.on('userInfo', (data) => {
console.log(data);
   socket.username = data.username;
   console.log(socket.username);
   let lobbyCheck = lobby.some(obj => obj.username === socket.username);
    if (!lobbyCheck) {
        lobby.push(socket);
         let playersCheck = readyPlayers.some(obj => obj.username === socket.username);

        if (!playersCheck) {
          readyPlayers.push(socket);

        }
        if (readyPlayers.length === 2) {

           let username1 = readyPlayers[0].username;
           let username2 = readyPlayers[1].username;
           let sessionId = randomUUID();
           let newGameObject = {
                id: sessionId,
                usernames: []
              };

           newGameObject["usernames"][0] = {username: username1, points: 0}
           newGameObject["usernames"][1] = {username: username2, points: 0}
           playingGames["koznazna"] = {}
           playingGames["koznazna"][sessionId] = {
                            0: [null,null,null],
                            1: [null,null,null],
                            2: [null,null,null],
                            3: [null,null,null],
                            4: [null,null,null],
                            5: [username1,username2],
                            6: [readyPlayers[0],readyPlayers[1]]
           }
            playingGames["korakpokorak"] = {}
            playingGames["korakpokorak"][sessionId] = {
                                0: [false,false],

               }
          let sendObjectFirebase = {
                    id: sessionId,
                    usernames: {
                    [username1]: 0,
                    [username2]: 0
                    }
                  };

           playingGames[sessionId] = [username1,username2];

          // Push the newGameObject to the database
          const databaseRef = firebaseAdmin.database().ref('gameSessions');
          databaseRef.child(sessionId).push(sendObjectFirebase)
            .then((snapshot) => {
              // Emit 'startGame' event to both players with the game session ID
              const gameSessionId = snapshot.key;
              readyPlayers.forEach(player => player.emit('startGame', newGameObject));

              // Remove players from the lobby and readyPlayers
              lobby.splice(lobby.indexOf(readyPlayers[0]), 1);
              lobby.splice(lobby.indexOf(readyPlayers[1]), 1);
              readyPlayers.splice(0, 2);
            })
            .catch((error) => {
              console.error('Error creating game session:', error);
            });
        }
      }
    });

socket.on('koznazna', (data) => {

console.log("number game"+data.questionNumber);
        let questionNumber = data.questionNumber;
        let answer = data.answer;
        let username = data.username;
        let sessionId = data.sessionId;

    let testObject = {number: questionNumber,answer: answer,username: username, sessionId: sessionId};

        let indexOfUser = playingGames["koznazna"][sessionId][5].indexOf(username);
        if (answer){
            if (!playingGames["koznazna"][sessionId][questionNumber].includes(true)){
                 playingGames["koznazna"][sessionId][questionNumber][2]= indexOfUser;
            }
        }
        playingGames["koznazna"][sessionId][questionNumber][indexOfUser] = answer
});


socket.on('koznaznapoints', (data) => {
console.log("Question number"+data.questionNumber);
        let questionNumber = data.questionNumber;
        let sessionId = data.sessionId;
        let arrayPoints = [data.pointsList[0],data.pointsList[1]];
        let arrayQuestion = playingGames["koznazna"][sessionId][questionNumber];

        console.log(arrayQuestion[2]);
        if (arrayQuestion[2] !== null) {
            console.log(arrayQuestion[2])
            arrayPoints[arrayQuestion[2]] = arrayPoints[arrayQuestion[2]] + 10;

            if (arrayQuestion[0] !== null && !arrayQuestion[0] ) {
              arrayPoints[0] =   arrayPoints[0] - 5;
            }
             if (arrayQuestion[1] !== null && !arrayQuestion[1] ) {
               arrayPoints[1] =  arrayPoints[1] - 5;
            }


            }


        else {
        if (arrayQuestion[0] !== null && !arrayQuestion[0] ) {
            arrayPoints[0] =   arrayPoints[0] - 5;
          }
           if (arrayQuestion[1] !== null && !arrayQuestion[1] ) {
             arrayPoints[1] =   arrayPoints[1] - 5;
          }
        }
       console.log("Points 1 "+arrayPoints[0]+"Points 2 "+arrayPoints[1])
       socket.emit('pointsUpdate',
         {0: arrayPoints[0],1: arrayPoints[1]});
});


socket.on('mojbrojgame', (data) => {
    let sessionId = data.sessionId;

    playingGames["mojbroj"] = {}
    playingGames["mojbroj"][sessionId] = {
                            0: [false,false],
                            1: [0,0,0],
                            2: [0,0,0],
           }
    let username = data.username;
    let indexOfUser = playingGames["koznazna"][sessionId][5].indexOf(username);
    if (indexOfUser != -1){
        playingGames["koznazna"][sessionId][6][indexOfUser] = socket;
    }
    console.log("Index"+indexOfUser)
});

socket.on('mojbrojnumber', (data) => {
    let number = data.number;
    let sessionId = data.sessionId;
    let roundNumber = data.round+1;

    if (roundNumber > 2){ //nekad runda bude 3
        roundNumber = 2;
    }


    for (let userSocket of playingGames["koznazna"][sessionId][6]){
        userSocket.emit("mojbrojupdate",{"number": number})
        playingGames["mojbroj"][sessionId][roundNumber][2] = number;
        console.log(number);
    }
});

socket.on('mojbrojcalculating', (data) => {
    let sessionId = data.sessionId;

    dataToSend = {
         "firstNumber": data.firstNumber,
         "secondNumber": data.secondNumber,  //drugi stop
         "thirdNumber":  data.thirdNumber,
         "fourthNumber":  data.fourthNumber,
         "specialNumber1":  data.specialNumber1,
         "specialNumber2":  data.specialNumber2
    }

    for (let userSocket of playingGames["koznazna"][sessionId][6]){
        userSocket.emit("mojbrojupdatecalculating",dataToSend)
    }
});

socket.on('mojbrojconfirm', (data) => {
    let username = data.username;
    let sessionId = data.sessionId;
    let roundNumber = data.roundNumber;
    let result = data.result;
    console.log(result)
    let indexOfUser = playingGames["koznazna"][sessionId][5].indexOf(username);
    if (indexOfUser != -1){
           playingGames["mojbroj"][sessionId][0][indexOfUser] = true;
           playingGames["mojbroj"][sessionId][roundNumber][indexOfUser] = result;
    }
    if (playingGames["mojbroj"][sessionId][0][0] && playingGames["mojbroj"][sessionId][0][1]){
        for (let userSocket of playingGames["koznazna"][sessionId][6]){
            userSocket.emit("mojbrojconfirmupdate",{"value": true})
        }

        playingGames["mojbroj"][sessionId][0][0] = false;
        playingGames["mojbroj"][sessionId][0][1] = false;
        let result1 = playingGames["mojbroj"][sessionId][roundNumber][0];
        let result2 = playingGames["mojbroj"][sessionId][roundNumber][1];
        let correctAnswer = playingGames["mojbroj"][sessionId][roundNumber][2];
        let user0 = 0;
        let user1 = 0
        if (roundNumber == 1){
            let answered = false;
            if (result1 === correctAnswer){
                user0 = 20;
                answered = true;
            } else if(result2 === correctAnswer){
                user1 = 20;
                answered = true;
            }

            if (!answered){
                if (result1 !== 0 && result2 !== 0){
                    if (result1 === result2){
                        user0 = 5;
                    } else {
                        let firstEq = Math.abs(result1-correctAnswer);
                        let secondEq = Math.abs(result2-correctAnswer);

                        user1 = 5;
                        if (firstEq<secondEq){
                            user0 = 5;
                            user1 = 0;
                        }
                    }
                }

            }

        }else {
             let answered = false;
              if (result2 === correctAnswer){
                 user1 = 20;
                  answered = true;
               } else if (result1 === correctAnswer){
                 user0 = 20;
                 answered = true;
               }

               if (!answered){
           if (result1 !== 0 && result2 !== 0){
               if (result1 === result2){
                   user1 = 5;
               } else {
                   let firstEq = Math.abs(result1-correctAnswer);
                   let secondEq = Math.abs(result2-correctAnswer);

                   user1 = 5;
                   if (firstEq<secondEq){
                       user0 = 5;
                       user1  = 0;
                   }
               }
           }
        }

        }

        for (let userSocket of playingGames["koznazna"][sessionId][6]){
         userSocket.emit("pointsUpdate",{0: user0,1: user1})
         console.log(0+user0)
         console.log(1+user1)
         }
    }

});

socket.on('korakpokorakgame', (data) => {
    let sessionId = data.sessionId;


    let username = data.username;
    let indexOfUser = playingGames["koznazna"][sessionId][5].indexOf(username);
    if (indexOfUser != -1){
        playingGames["koznazna"][sessionId][6][indexOfUser] = socket;
        playingGames["korakpokorak"][sessionId][0][indexOfUser] = true;
    }
      if (playingGames["korakpokorak"][sessionId][0][0] &&
            playingGames["korakpokorak"][sessionId][0][1]){

        var wordsRef = firebaseAdmin.database().ref("games/korapokorak/binbr");

        wordsRef.once("value", function(dataSnapshot) {
          if (dataSnapshot.exists()) {
            var answer = dataSnapshot.child("answer").val();
            var words = [];
            var wordsSnapshot = dataSnapshot.child("words");
            wordsSnapshot.forEach(function(wordSnapshot) {
              var word = wordSnapshot.val();
              words.push(word);
              console.log(word);
            });
            game = {
              answer: answer,
              words: words
            };

         for (let userSocket of playingGames["koznazna"][sessionId][6]){
                userSocket.emit("korakpokorakdata",game)
          }
          } else {

          }
        }, function(error) {
          // Failed to read
          // console.log("Failed to read: " + error.message);
        });
        }
});


socket.on('korakpokorakreset', (data) => {
    let sessionId = data.sessionId;
    let roundNumber = data.roundNumber;

    var wordsRef = firebaseAdmin.database().ref("games/korapokorak/kyb");
    wordsRef.once("value", function(dataSnapshot) {
          if (dataSnapshot.exists()) {
            var answer = dataSnapshot.child("answer").val();
            var words = [];
            var wordsSnapshot = dataSnapshot.child("words");
            wordsSnapshot.forEach(function(wordSnapshot) {
              var word = wordSnapshot.val();
              words.push(word);
              console.log(word);
            });
            game = {
              answer: answer,
              words: words,
              roundNumber: roundNumber,
            };

         for (let userSocket of playingGames["koznazna"][sessionId][6]){
                userSocket.emit("korakpokorakresetgame",game)
          }
          }
        }, function(error) {
          // Failed to read
          // console.log("Failed to read: " + error.message);
        });

});

socket.on('korakpokoraksecondtry', (data) => {
    let sessionId = data.sessionId;
    let roundNumber = data.roundNumber;


     for (let userSocket of playingGames["koznazna"][sessionId][6]){
            userSocket.emit("korakpokoraksecond",{roundNumber: roundNumber})
      }
});

socket.on('korakpokorakPoints', (data) => {
    let sessionId = data.sessionId;
    let username = data.username;
    let points = data.points;

    console.log(username);

    let indexOfUser = playingGames["koznazna"][sessionId][5].indexOf(username);
        if (indexOfUser != -1){
         for (let userSocket of playingGames["koznazna"][sessionId][6]){
                    userSocket.emit("korakpokorakPointsupdate",{points: points,username: username})
              }
        }


});
socket.on('nextGame', (data) => {

     for (let userSocket of playingGames["koznazna"][data.sessionId][6]){
                userSocket.emit("nextGameSend",{})
          }

});

  socket.on('disconnect', () => {
    console.log(`Player disconnected: ${socket.id}`);

    // Remove player from the lobby and readyPlayers (if they were ready)
    const lobbyIndex = lobby.findIndex(user => user.userId === socket.userId);
    if (lobbyIndex !== -1) {
      lobby.splice(lobbyIndex, 1);
    }

    const readyPlayersIndex = readyPlayers.indexOf(socket);
    if (readyPlayersIndex !== -1) {
      readyPlayers.splice(readyPlayersIndex, 1);
    }

    // Send updated lobby data to all connected clients
    io.emit('updateLobby', lobby);
  });
  });

  // Handle player readiness for the game




// ...
